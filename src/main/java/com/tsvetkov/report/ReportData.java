package com.tsvetkov.report;

import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.model.entity.Receipt;

import java.util.List;
import java.util.Map;

public interface ReportData extends Comparable<ReportData> {
    String getName();
    Map<String,Double> getMonthTurnoverMap();
    void turnover(int month, double turnoverValue);
    double getTurnover();
    void setTurnOver(double turnover);
    void setName(String name);
    void calculateTurnover();
    void turnOverPerDay(int day, double calculateTurnover);
    double calculateTurnoverForReceiptAndInvoice(List<Receipt> receipts, List<Invoice> invoices);
    List<String> getNames();
}
