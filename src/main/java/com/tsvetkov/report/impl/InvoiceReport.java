package com.tsvetkov.report.impl;

import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.model.entity.Company;
import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.model.entity.Store;
import com.tsvetkov.report.ReportData;
import com.tsvetkov.report.ReportEngine;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InvoiceReport implements ReportEngine {
  private final ReportService reportService = ReportService.getInstance();
  private final ReportData reportData = new ReportDataImpl();
  private final Report report = Report.getInstance();
  private final Connection connection = ConnectionManager.getConnection().orElseThrow();
  private Company company;
  private List<Store> stores;

  @Override
  public void createQuarterAndYearReport(List<Integer> months, int year, String companyName, String order, int top)
      throws SQLException {
    this.company =
        reportService
            .getCompanyService()
            .findByCompanyName("name", companyName, connection)
            .orElseThrow();

    this.stores = this.reportService.getStoreService().findStoreByCompanyId(company.getId(), connection);

    for (Integer month : months) {
      List<Invoice> addAllInvoices = new ArrayList<>();
      for (Store store : stores) {

        List<Invoice> invoices =
            this.reportService.getInvoiceService().findByDateAndStoreId(year, month, store.getId());
        addAllInvoices.addAll(invoices);
        invoices.clear();
      }
      this.reportData.turnover( month, this.reportData.calculateTurnoverForReceiptAndInvoice(
              Collections.EMPTY_LIST, addAllInvoices));
    }
    this.reportData.calculateTurnover();
    report.getReportData().add(reportData);

    report.setCellFields("INVOICE");
  }

  @Override
  public void createMonthlyReport(List<Integer> months, int year, String companyName, String order , int top)
      throws SQLException {
    this.company =
        this.reportService
            .getCompanyService()
            .findByCompanyName("name", companyName, connection)
            .orElseThrow();
    stores = reportService.getStoreService().findStoreByCompanyId(company.getId(), connection);

    YearMonth yearMonth = YearMonth.of(year, months.get(0));
    int mothDays = yearMonth.lengthOfMonth();
    for (int day = 1; day <= mothDays; day++) {

      List<Invoice> addAllReceipts = new ArrayList<>();
      for (Store store : stores) {
        List<Invoice> receiptByDayOfMonth = reportService.getInvoiceService()
                .findByDayOfMonth(year, months.get(0), day, store.getId());

        addAllReceipts.addAll(receiptByDayOfMonth);
        receiptByDayOfMonth.clear();
      }
      this.reportData.turnOverPerDay(day,
          this.reportData.calculateTurnoverForReceiptAndInvoice(
                  Collections.EMPTY_LIST, addAllReceipts));
    }
    this.reportData.calculateTurnover();
    report.getReportData().add(reportData);

    report.setCellFields("INVOICE");

  }
}
