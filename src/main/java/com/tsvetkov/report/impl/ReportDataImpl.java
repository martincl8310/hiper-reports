package com.tsvetkov.report.impl;

import com.tsvetkov.constant.AppConstants;
import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.model.entity.Receipt;
import com.tsvetkov.report.ReportData;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ReportDataImpl implements ReportData {
  private String name;
  private Map<String, Double> monthTurnoverMap;
  private double turnover;
  private List<String> names = new ArrayList<>();

  public ReportDataImpl() {
    this.monthTurnoverMap = new LinkedHashMap<>();
    this.setTurnOver(turnover);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Map<String, Double> getMonthTurnoverMap() {
    return monthTurnoverMap;
  }

  public double getTurnover() {
    return turnover;
  }

  @Override
  public void setTurnOver(double turnover) {
    this.turnover = turnover;
  }

  @Override
  public void turnover(int month, double turnover) {
    this.monthTurnoverMap.put(AppConstants.MONTHS_OF_THE_YEAR[month - 1], turnover);
  }

  @Override
  public void calculateTurnover() {
    for (Map.Entry<String, Double> month : monthTurnoverMap.entrySet()) {
      this.turnover += month.getValue();
    }
  }

  @Override
  public void turnOverPerDay(int day, double calculateTurnover) {
    this.monthTurnoverMap.put(String.valueOf(day), calculateTurnover);
  }

  @Override
  public int compareTo(ReportData reportData) {
    return Double.compare(this.turnover, reportData.getTurnover());
  }

  @Override
  public double calculateTurnoverForReceiptAndInvoice(
      List<Receipt> receipts, List<Invoice> invoices) {
    double total = 0;
    for (Receipt addAllReceipt : receipts) {
      double receiptTotal = Double.parseDouble(String.valueOf(addAllReceipt.getTotal()));
      total += receiptTotal;
    }
    for (Invoice invoice : invoices) {
      final double totalInvoice = Double.parseDouble(String.valueOf(invoice.getTotal()));
      total += totalInvoice;
    }
    return total;
  }

  @Override
  public List<String> getNames() {
    return names;
  }
}
