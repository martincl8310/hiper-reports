package com.tsvetkov.report.impl;

import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.model.entity.Company;
import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.model.entity.Receipt;
import com.tsvetkov.report.ReportData;
import com.tsvetkov.report.ReportEngine;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

public class PaymentReport implements ReportEngine {
  private final ReportService reportService = ReportService.getInstance();
  private final Report report = Report.getInstance();
  private final Connection connection =
      ConnectionManager.getConnection().orElseThrow(IllegalStateException::new);

  @Override
  public void createQuarterAndYearReport(
      List<Integer> months, int year, String companyName, String order, int top)
      throws SQLException {

    Company company =
        reportService
            .getCompanyService()
            .findByCompanyName("name", companyName, connection)
            .orElseThrow();

    List<String> payment = new ArrayList<>();
    payment.add("cash");
    payment.add("card");
    int count = 0;

    while (true){
      if (count > 1){
        break;
      }
      ReportData reportData = new ReportDataImpl();
      List<Receipt> receiptsCash = new ArrayList<>();
      List<Invoice> invoiceCash = new ArrayList<>();
      for (Integer month : months) {

        receiptsCash =
                this.reportService.getReceiptService().findReceiptByPaymentAndMonth(year, month, payment.get(count), company.getId());

        invoiceCash =
                this.reportService.getInvoiceService().findByInvoicePaymentAndMonth(year, month, payment.get(count), company.getId());

        reportData.turnover(month, reportData.calculateTurnoverForReceiptAndInvoice(receiptsCash, invoiceCash));
      }
      reportData.calculateTurnover();
      reportData.getNames().add(payment.get(count));
      report.getReportData().add(reportData);
      receiptsCash.clear();
      invoiceCash.clear();
      count++;
    }
    report.setCellFields("PAYMENT");
  }

  @Override
  public void createMonthlyReport(
      List<Integer> months, int year, String companyName, String order, int top) throws SQLException {

    Company company =
            reportService
                    .getCompanyService()
                    .findByCompanyName("name", companyName, connection)
                    .orElseThrow();

    List<String> payment = new ArrayList<>();
    payment.add("cash");
    payment.add("card");
    int count = 0;

    while (true){
      if (count > 1){
        break;
      }
      ReportData reportData = new ReportDataImpl();
      List<Receipt> receiptsCash = new ArrayList<>();
      List<Invoice> invoiceCash = new ArrayList<>();

      YearMonth yearMonth = YearMonth.of(year, months.get(0));
      int monthDays = yearMonth.lengthOfMonth();
      for (int day = 1; day <= monthDays; day++) {

        receiptsCash =
                this.reportService.getReceiptService().findReceiptByPaymentPerDayAndMonth(year, months.get(0), day, payment.get(count), company.getId());

        invoiceCash =
                this.reportService.getInvoiceService().findInvoiceByPaymentPerDay(year, months.get(0),day , payment.get(count), company.getId());

        reportData.turnOverPerDay(day, reportData.calculateTurnoverForReceiptAndInvoice(receiptsCash, invoiceCash));
      }
      reportData.calculateTurnover();
      reportData.getNames().add(payment.get(count));
      report.getReportData().add(reportData);
      receiptsCash.clear();
      invoiceCash.clear();
      count++;
    }
    report.setCellFields("PAYMENT");

  }
}
