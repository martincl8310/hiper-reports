package com.tsvetkov.report.impl;

import com.tsvetkov.model.entity.Company;
import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.model.entity.Receipt;
import com.tsvetkov.model.entity.Store;
import com.tsvetkov.repository.impl.InvoiceRepository;
import com.tsvetkov.repository.impl.JdbcRepository;
import com.tsvetkov.repository.impl.ReceiptRepository;
import com.tsvetkov.repository.impl.StoreRepository;
import com.tsvetkov.service.CompanyService;
import com.tsvetkov.service.InvoiceService;
import com.tsvetkov.service.ReceiptService;
import com.tsvetkov.service.StoreService;
import com.tsvetkov.service.impl.CompanyServiceImpl;
import com.tsvetkov.service.impl.InvoiceServiceImpl;
import com.tsvetkov.service.impl.ReceiptServiceImpl;
import com.tsvetkov.service.impl.StoreServiceImpl;

public class ReportService {
  private static final ReportService instance = new ReportService();
  private final ReceiptService receiptService;
  private final CompanyService companyService;
  private final StoreService storeService;
  private final InvoiceService invoiceService;

  private ReportService(){
    this.invoiceService = new InvoiceServiceImpl(new InvoiceRepository(Invoice.class));
    this.receiptService = new ReceiptServiceImpl(new ReceiptRepository(Receipt.class));
    this.companyService = new CompanyServiceImpl(new JdbcRepository<>(Company.class));
    this.storeService = new StoreServiceImpl(new StoreRepository(Store.class));
  }

  public ReceiptService getReceiptService() {
    return receiptService;
  }

  public CompanyService getCompanyService() {
    return companyService;
  }

  public StoreService getStoreService() {
    return storeService;
  }

  public InvoiceService getInvoiceService() {
    return invoiceService;
  }

  public static ReportService getInstance(){
    return instance;
  }
}
