package com.tsvetkov.report.impl;

import com.tsvetkov.report.ReportData;

import java.util.ArrayList;
import java.util.List;

public class Report {
  private static final Report instance = new Report();
  private List<String> cellFields;
  private List<ReportData> reportData = new ArrayList<>();
  private int top;

  private Report() {
    this.setTop(top);
    this.setReportData(this.reportData);
  }

  public List<ReportData> getReportData() {
    return reportData;
  }

  public void setReportData(List<ReportData> reportData) {
    if (top != 0 || top == reportData.size()) {
      this.reportData = reportData.subList(0, top);
    } else {
      this.reportData = reportData;
    }
  }

  public void setCellFields(String firstField) {
    this.cellFields = new ArrayList<>();

    cellFields.add(firstField);
    this.cellFields.addAll(reportData.get(0).getMonthTurnoverMap().keySet());
    this.cellFields.add("TURNOVER");
  }

  public List<String> getCellFields() {
    return this.cellFields;
  }

  @Override
  public String toString() {
    return "Report{" + "cellFields=" + cellFields + ", reportDataList=" + reportData + '}';
  }

  public int getTop() {
    return top;
  }

  public void setTop(int top) {
    this.top = top;
  }

  public static Report getInstance() {
    return instance;
  }
}
