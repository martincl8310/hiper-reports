package com.tsvetkov.report.impl;

import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.model.entity.Company;
import com.tsvetkov.model.entity.Receipt;
import com.tsvetkov.model.entity.Store;
import com.tsvetkov.report.ReportData;
import com.tsvetkov.report.ReportEngine;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReceiptReport implements ReportEngine {
    private final ReportService reportService = ReportService.getInstance();
    private final ReportData reportData = new ReportDataImpl();
    private final Report report = Report.getInstance();

    @Override
    public void createQuarterAndYearReport(List<Integer> months, int year, String companyName, String order, int top) throws SQLException {

    Connection connection = ConnectionManager.getConnection().orElseThrow();
    Company company =
        reportService.getCompanyService().findByCompanyName("name", companyName, connection).orElseThrow();

    List<Store> stores = this.reportService.getStoreService().findStoreByCompanyId(company.getId(), connection);

    for (Integer month : months) {

      List<Receipt> addAllReceipts = new ArrayList<>();
      for (Store store : stores) {

        List<Receipt> receipts =
            this.reportService.getReceiptService().findByDateAndStoreId(year, month, store.getId());
        addAllReceipts.addAll(receipts);
        receipts.clear();
      }

      this.reportData.turnover(
          month, this.reportData.calculateTurnoverForReceiptAndInvoice(addAllReceipts, Collections.EMPTY_LIST));
    }
    this.reportData.calculateTurnover();
    report.getReportData().add(reportData);

    report.setCellFields("RECEIPT");
    }

    @Override
    public void createMonthlyReport(List<Integer> months, int year, String companyName, String order, int top) throws SQLException {

    Connection connection = ConnectionManager.getConnection().orElseThrow();
    Company company =
        this.reportService.getCompanyService()
                .findByCompanyName("name", companyName, connection).orElseThrow();
    List<Store> stores = reportService.getStoreService().findStoreByCompanyId(company.getId(), connection);

    YearMonth yearMonth = YearMonth.of(year, months.get(0));
    int mothDays = yearMonth.lengthOfMonth();

    for (int day = 1; day <= mothDays; day++) {

      List<Receipt> addAllReceipts = new ArrayList<>();
      for (Store store : stores) {
        List<Receipt> receiptByDayOfMonth =
                reportService.getReceiptService().findReceiptByDayOfMonth(year, months.get(0),day, store.getId());

        addAllReceipts.addAll(receiptByDayOfMonth);
        receiptByDayOfMonth.clear();
      }
      this.reportData.turnOverPerDay(day, this.reportData.calculateTurnoverForReceiptAndInvoice(addAllReceipts, Collections.EMPTY_LIST));
    }
    this.reportData.calculateTurnover();
    report.getReportData().add(reportData);

    report.setCellFields("RECEIPT");

    }

}
