package com.tsvetkov.report.impl;

import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.model.entity.Company;
import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.model.entity.Receipt;
import com.tsvetkov.model.entity.Store;
import com.tsvetkov.report.ReportData;
import com.tsvetkov.report.ReportEngine;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.YearMonth;
import java.util.Collections;
import java.util.List;

public class StoreReport implements ReportEngine {
  private final ReportService reportService = ReportService.getInstance();
  private final Report report = Report.getInstance();
  private final Connection connection = ConnectionManager.getConnection().orElseThrow(IllegalStateException::new);

  @Override
  public void createQuarterAndYearReport(List<Integer> months, int year, String companyName, String order, int top)
      throws SQLException {
    Company company = reportService
            .getCompanyService()
            .findByCompanyName("name", companyName, connection)
            .orElseThrow();

    List<Store> stores = this.reportService.getStoreService().findStoreByCompanyId(company.getId(), connection);

    for (Store store : stores) {
      ReportData reportData = new ReportDataImpl();

      for (Integer month : months) {

        List<Receipt> receipts =
                this.reportService.getReceiptService().findByDateAndStoreId(year, month, store.getId());

        List<Invoice> invoices =
                this.reportService.getInvoiceService().findByDateAndStoreId(year, month, store.getId());

        reportData.turnover(month, reportData.calculateTurnoverForReceiptAndInvoice(receipts, invoices));

        receipts.clear();
        invoices.clear();
      }
      reportData.calculateTurnover();
      reportData.getNames().add(store.getName());

      report.getReportData().add(reportData);
    }
    this.report.setTop(top);
    report.setCellFields("STORE");

    if (order == null || order.equals("asc")){
      Collections.sort(report.getReportData());
    }else if (order.equals("desc")){
      Collections.reverse(report.getReportData());
    }
    this.report.setReportData(report.getReportData());
  }

  @Override
  public void createMonthlyReport(List<Integer> months, int year, String companyName, String order, int top)
      throws SQLException {
    Company company = reportService
            .getCompanyService()
            .findByCompanyName("name", companyName, connection)
            .orElseThrow();

    List<Store> stores = this.reportService.getStoreService().findStoreByCompanyId(company.getId(), connection);

    for (Store store : stores) {
      ReportData reportData = new ReportDataImpl();
      YearMonth yearMonth = YearMonth.of(year, months.get(0));
      int mothDays = yearMonth.lengthOfMonth();

      for (int day = 1; day <= mothDays; day++) {

        List<Receipt> receipts =
                this.reportService.getReceiptService().findReceiptByDayOfMonth(year, months.get(0),day, store.getId());

        List<Invoice> invoices =
                this.reportService.getInvoiceService().findByDayOfMonth(year, months.get(0), day, store.getId());

        reportData.turnOverPerDay(day, reportData.calculateTurnoverForReceiptAndInvoice(receipts, invoices));
        receipts.clear();
        invoices.clear();
      }
      reportData.calculateTurnover();
      reportData.getNames().add(store.getName());
      report.getReportData().add(reportData);
    }

    this.report.setTop(top);
    if (order == null || order.equals("asc")){
      Collections.sort(report.getReportData());
    }else if (order.equals("desc")){
      Collections.reverse(report.getReportData());
    }
    this.report.setReportData(report.getReportData());
    report.setCellFields("STORE");
  }
}
