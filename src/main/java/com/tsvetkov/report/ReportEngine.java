package com.tsvetkov.report;

import java.sql.SQLException;
import java.util.List;

public interface ReportEngine {

  void createQuarterAndYearReport(
      List<Integer> months, int year, String companyName, String order, int top)
      throws SQLException;

  void createMonthlyReport(
      List<Integer> months, int year, String companyName, String order, int top)
      throws SQLException;
}
