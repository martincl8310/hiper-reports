package com.tsvetkov.util.sqlutils.utils;

public class Where implements Query {

  private QueryInfo queryInfo;


  Where(QueryInfo queryInfo) {
    this.queryInfo = queryInfo;
    this.queryInfo.append("WHERE ");
  }

  public Where column(String column, WhereOperator operator) {
    String condition = column + " " + operator.getValue() + " ?";
    queryInfo.append(condition);
    return this;
  }

  public Where and() {
    queryInfo.append(" AND ");
    return this;
  }

  public Where or() {
    queryInfo.append(" OR ");
    return this;
  }
  public GroupBy groupBy(){
    return new GroupBy(this.queryInfo);
  }

  @Override
  public QueryInfo build() {
    return queryInfo;
  }
}
