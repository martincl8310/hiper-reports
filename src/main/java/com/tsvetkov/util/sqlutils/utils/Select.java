package com.tsvetkov.util.sqlutils.utils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Select {

  private QueryInfo queryInfo;
  private List<String> columns;

  Select(QueryInfo queryInfo) {
    this.queryInfo = queryInfo;
    this.queryInfo.append("SELECT ");
    columns = new LinkedList<>();
  }

  public Select all() {
    queryInfo.append("*");
    return this;
  }

  public Select column(String column) {
    columns.add(column);
    return this;
  }

  public Select column(String... columns) {
    this.columns.addAll(Arrays.asList(columns));
    return this;
  }

  public From from() {
    queryInfo.appendLine(QueryBuilder.processColumns(columns));
    return new From(queryInfo);
  }
}
