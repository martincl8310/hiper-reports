package com.tsvetkov.util.sqlutils.utils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class GroupBy implements Query {
    private QueryInfo queryInfo;
    private List<String> columns;
    GroupBy(QueryInfo queryInfo) {
        this.queryInfo = queryInfo;
        this.queryInfo.append(" GROUP BY ");
        this.columns = new LinkedList<>();
    }
    public GroupBy column(String column) {
        columns.add(column);
        return this;
    }
    public GroupBy column(String... columns) {
        this.columns.addAll(Arrays.asList(columns));
        return this;
    }
//    public OrderBy orderBy() {
//        queryInfo.appendLine(QueryBuilder.processColumns(columns));
//        return new OrderBy(queryInfo);
//    }
    @Override
    public QueryInfo build() {
        queryInfo.appendLine(QueryBuilder.processColumns(columns));
        return this.queryInfo;
    }
}
