package com.tsvetkov.util.sqlutils.utils;

import java.util.HashMap;
import java.util.Map;

public class QueryInfo {
  Map<Integer, Object> placeholders = new HashMap<>();

  private  StringBuilder sql;

  private String query;

  public QueryInfo() {
    this.sql = new StringBuilder();
  }

  public QueryInfo(QueryInfo clone) {
    this.sql = clone.sql;
  }

  QueryInfo append(String expression) {
    sql.append(expression);
    return this;
  }

  QueryInfo appendLine(String expression) {
    sql.append(expression).append("\n");
    return this;
  }

  public String getQuery() {
    return sql.toString();
  }


  public Map<Integer, Object> getPlaceholders() {
    return placeholders;
  }

  public void setPlaceholders(Map<Integer, Object> placeholders) {
    this.placeholders = placeholders;
  }

  public StringBuilder getSql() {
    return sql;
  }

  public void setQuery(String query) {
    this.query = query;
  }
}
