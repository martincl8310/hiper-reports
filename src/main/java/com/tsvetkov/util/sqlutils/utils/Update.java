package com.tsvetkov.util.sqlutils.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Update {
  private QueryInfo queryInfo;
  private List<String> columns;

  public Update(QueryInfo queryInfo) {
    this.queryInfo = queryInfo;
    this.queryInfo.append("UPDATE ");
    this.columns = new ArrayList<>();
  }

  public Update updateTable(String table) {
    queryInfo.append(table);
    return this;
  }

  public Set set() {
    queryInfo.appendLine(QueryBuilder.processColumns(columns));
    return new Set(queryInfo);
  }

  public Update column(String column) {
    columns.add(column);
    return this;
  }

  public Update columns(Collection<String> columns) {
    List<String> placeholders = columns.stream().map(column -> "?").collect(Collectors.toList());
    queryInfo.append(" SET = " + QueryBuilder.processColumns(placeholders) + " ");
    return this;
  }

  public Where where() {
    return new Where(queryInfo);
  }
}
