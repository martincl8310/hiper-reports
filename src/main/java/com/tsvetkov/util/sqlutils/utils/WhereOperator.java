package com.tsvetkov.util.sqlutils.utils;

public enum WhereOperator {
  EQ("="),
  GT(">"),
  GTE(">="),
  LT("<"),
  LTE("<=");

  private String value;

  WhereOperator(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
