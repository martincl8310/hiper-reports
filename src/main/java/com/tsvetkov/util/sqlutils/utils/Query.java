package com.tsvetkov.util.sqlutils.utils;

public interface Query {
  QueryInfo build();
}
