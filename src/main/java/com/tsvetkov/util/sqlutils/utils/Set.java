package com.tsvetkov.util.sqlutils.utils;

public class Set implements Query {
  private QueryInfo queryInfo;

  Set(QueryInfo queryInfo) {
    this.queryInfo = queryInfo;
    this.queryInfo.append("SET ");
  }

  public Set columns(String column) {
    queryInfo.appendLine(column + " = ? ");
    return this;
  }

  public Where where() {
    return new Where(queryInfo);
  }

  @Override
  public QueryInfo build() {
    return queryInfo;
  }
}
