package com.tsvetkov.util.sqlutils.utils;

public enum JoinType {
  INNER_JOIN(" INNER JOIN "),
  LEFT_JOIN(" LEFT JOIN "),
  RIGHT_JOIN(" RIGHT JOIN ");

  private String value;

  JoinType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
