package com.tsvetkov.util.sqlutils.utils;

public class Join implements Query {

  private QueryInfo queryInfo;

  Join(QueryInfo queryInfo, JoinType joinType) {
    this.queryInfo = queryInfo;
    queryInfo.append(joinType.getValue());
  }

  public Join table(String table) {
    queryInfo.appendLine(table);
    return this;
  }

  public Join on(String leftOperand, String rightOperand) {
    queryInfo.appendLine("ON " + leftOperand + "=" + rightOperand);
    return this;
  }

  public Join join(JoinType joinType){
    return new Join(queryInfo, joinType);
  }

  public Where where() {
    return new Where(queryInfo);
  }

  @Override
  public QueryInfo build() {
    return queryInfo;
  }
}
