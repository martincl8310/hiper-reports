package com.tsvetkov.util.sqlutils.utils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Insert implements Query {
  private QueryInfo queryInfo;

  Insert(QueryInfo queryInfo) {
    this.queryInfo = queryInfo;
    this.queryInfo.append("INSERT INTO ");
  }

  public Insert intoTable(String table) {
    queryInfo.append(table);
    return this;
  }

  public Insert columns(Collection<String> columns) {
    queryInfo.append(" (" + QueryBuilder.processColumns(columns) + ")");

    List<String> placeholders = columns.stream().map(column -> "?").collect(Collectors.toList());

    queryInfo.append(" VALUES (" + QueryBuilder.processColumns(placeholders) + ")");
    return this;
  }

  @Override
  public QueryInfo build() {
    return queryInfo;
  }
}
