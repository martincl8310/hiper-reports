package com.tsvetkov.util.sqlutils;

import com.tsvetkov.util.sqlutils.utils.*;

import java.util.List;

public class SqlUtil {

  public QueryInfo projection(String tableName, List<String> columns) {
    Select select = QueryBuilder.builder().select();

    if (columns == null || columns.isEmpty()) select.all();
    else select.column(columns.toArray(new String[0]));

    return select.from().table(tableName).build();
  }

  public QueryInfo update(String tableName, String columnToUpdate, String column, String operator) {
    return QueryBuilder.builder()
        .update()
        .updateTable(tableName)
        .set()
        .columns(columnToUpdate)
        .where()
        .column(column, WhereOperator.valueOf(operator))
        .build();
  }

  public QueryInfo selectByYearAndMonth(String tableName, String joinTableName) {
    return QueryBuilder.builder()
        .select()
        .column("sum(total)")
        .from()
        .table(String.format("%s", tableName))
        .join(JoinType.INNER_JOIN)
        .table(String.format("%s", joinTableName))
        .on(String.format("%s.store_id", tableName), String.format("%s.id", joinTableName))
        .where()
        .column(String.format("year(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("month(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("%s.id", joinTableName), WhereOperator.EQ)
        .groupBy()
        .column(String.format("%s.name", joinTableName))
        .build();
  }

  public QueryInfo selectByDayOfMonth(String tableName, String joinTableName) {
    return QueryBuilder.builder()
        .select()
        .column("sum(total)")
        .from()
        .table(String.format("%s", tableName))
        .join(JoinType.INNER_JOIN)
        .table(String.format("%s", joinTableName))
        .on(String.format("%s.store_id", tableName), String.format("%s.id", joinTableName))
        .where()
        .column(String.format("year(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("month(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("day(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("%s.id", joinTableName), WhereOperator.EQ)
        .groupBy()
        .column(String.format("%s.name", joinTableName))
        .build();
  }

  public QueryInfo selectionByPaymentPerMonth(
      String tableName, String joinTableName, String secondJoinTable) {
    return QueryBuilder.builder()
        .select()
        .column("sum(total)")
        .from()
        .table(String.format("%s", tableName))
        .join(JoinType.INNER_JOIN)
        .table(String.format("%s", joinTableName))
        .on(String.format("%s.store_id", tableName), String.format("%s.id", joinTableName))
        .join(JoinType.INNER_JOIN)
        .table(String.format("%s", secondJoinTable))
        .on(String.format("%s.id", secondJoinTable), String.format("%s.company_id", joinTableName))
        .where()
        .column(String.format("year(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("month(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("%s.payment", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("%s.id", secondJoinTable), WhereOperator.EQ)
        .groupBy()
        .column(String.format("%s.name", secondJoinTable))
        .build();
  }

  public QueryInfo selectionByPaymentPerMonthAndDay(
      String tableName, String joinTableName, String secondJoinTable) {
    return QueryBuilder.builder()
        .select()
        .column("sum(total)")
        .from()
        .table(String.format("%s", tableName))
        .join(JoinType.INNER_JOIN)
        .table(String.format("%s", joinTableName))
        .on(String.format("%s.store_id", tableName), String.format("%s.id", joinTableName))
        .join(JoinType.INNER_JOIN)
        .table(String.format("%s", secondJoinTable))
        .on(String.format("%s.id", secondJoinTable), String.format("%s.company_id", joinTableName))
        .where()
        .column(String.format("year(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("month(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("day(%s.local_date)", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("%s.payment", tableName), WhereOperator.EQ)
        .and()
        .column(String.format("%s.id", secondJoinTable), WhereOperator.EQ)
        .groupBy()
        .column(String.format("%s.name", secondJoinTable))
        .build();
  }

  public QueryInfo selection(String tableName, String column, String operator) {
    return QueryBuilder.builder()
        .select()
        .all()
        .from()
        .table(tableName)
        .where()
        .column(column, WhereOperator.valueOf(operator))
        .build();
  }

  public QueryInfo insert(String tableName, List<String> columns) {
    return QueryBuilder.builder().insert().intoTable(tableName).columns(columns).build();
  }
}
