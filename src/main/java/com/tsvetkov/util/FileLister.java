package com.tsvetkov.util;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface FileLister {
    List<Path> getFilePaths(String path) throws IOException;

}
