package com.tsvetkov.util;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface XmlDeserializer {

  <T> T unmarshallFromXml(Class<T> objectType, String filePath) throws JAXBException, IOException;
}
