package com.tsvetkov.util;

import java.sql.SQLException;

public interface DbTableCreator {
    void createTables() throws SQLException;
}
