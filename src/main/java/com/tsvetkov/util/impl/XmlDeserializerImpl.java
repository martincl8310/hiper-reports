package com.tsvetkov.util.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.tsvetkov.util.XmlDeserializer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class XmlDeserializerImpl implements XmlDeserializer {

  private static final XmlDeserializerImpl instance = new XmlDeserializerImpl();
  private Logger logger = LoggerFactory.getLogger(XmlDeserializerImpl.class);

  private XmlDeserializerImpl() {}

  public static XmlDeserializerImpl getInstance() {
    return instance;
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T> T unmarshallFromXml(Class<T> objectType, String path) throws JAXBException {
    try {
      JAXBContext context = JAXBContext.newInstance(objectType);
      Unmarshaller unmarshaller = context.createUnmarshaller();
      return (T) unmarshaller.unmarshal(new File(path));
    } catch (JAXBException e) {
      logger.error(e.getMessage());
      throw new JAXBException("Failed to unmarshal XML document");
    }
  }

}
