package com.tsvetkov.util.impl;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class ReflectionUtil {
    public static List<Field> getClassFields(Class<?> tClass, List<Field> fields) {
        fields.addAll(Arrays.asList(tClass.getDeclaredFields()));

        if (tClass.getSuperclass() != null)
            getClassFields(tClass.getSuperclass(), fields);

        return fields;
    }

}
