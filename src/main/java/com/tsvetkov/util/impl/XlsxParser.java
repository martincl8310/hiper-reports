package com.tsvetkov.util.impl;

import com.tsvetkov.configuration.DirectoryProperties;
import com.tsvetkov.report.ReportData;
import com.tsvetkov.report.impl.Report;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class XlsxParser {

  private static final XlsxParser instance = new XlsxParser();
  private XlsxParser(){}

  public void xlsxReport(Report report, String filename) throws IOException {
    DirectoryProperties directoryProperties = DirectoryProperties.getInstance();

    XSSFWorkbook workbook = new XSSFWorkbook();
    XSSFSheet sheet = workbook.createSheet("hyper-reports");

    int rowCounter = 0;
    int cellCounter = 0;
    List<String> cellFields = report.getCellFields();
    Row row = sheet.createRow(rowCounter++);

    for (String field : cellFields){
      row.createCell(cellCounter++).setCellValue(field);
    }

    cellCounter = 0;
    for (ReportData data : report.getReportData()) {
      row = sheet.createRow(rowCounter++);
      if (data.getNames().isEmpty()){
        row.createCell(cellCounter++).setCellValue(data.getName());
      }else {
        row.createCell(cellCounter++).setCellValue(data.getNames().get(0));
      }

      for (Map.Entry<String, Double> entry : data.getMonthTurnoverMap().entrySet()) {

        row.createCell(cellCounter++).setCellValue(entry.getValue());
      }
      row.createCell(cellCounter++).setCellValue(data.getTurnover());
      cellCounter = 0;
    }

    if (directoryProperties.getExportedDirectory().isEmpty()){
      System.out.println("NOT SET");
    }
    String path = directoryProperties.getExportedDirectory();

    if (directoryProperties.getExportedDirectory().charAt(directoryProperties.getExportedDirectory().length() - 1) != '/') {
       path = directoryProperties.getExportedDirectory()  + '/';
    }

    workbook.write(new FileOutputStream(path+ filename + ".xlsx"));
    workbook.close();
  }
  public static XlsxParser getInstance(){
    return instance;
  }
}
