package com.tsvetkov.util.impl;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;

public class LocalDateAdapter extends XmlAdapter<String, LocalDateTime> {
    public LocalDateTime unmarshal(String localDateTime) {
        return LocalDateTime.parse(localDateTime);
    }

    public String marshal(LocalDateTime localDateTime) {
        return localDateTime.toString();
    }
}
