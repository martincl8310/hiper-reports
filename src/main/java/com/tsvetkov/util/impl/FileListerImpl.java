package com.tsvetkov.util.impl;

import com.tsvetkov.util.FileLister;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class FileListerImpl implements FileLister {

  private static final FileListerImpl instance = new FileListerImpl();

  private FileListerImpl() {}

  @Override
  public List<Path> getFilePaths(String path) throws IOException {
    return Files.list(Paths.get(path)).collect(Collectors.toList());
  }

  public static FileListerImpl getInstance() {
    return instance;
  }
}
