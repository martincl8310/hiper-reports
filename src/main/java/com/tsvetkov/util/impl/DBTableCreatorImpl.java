package com.tsvetkov.util.impl;

import com.tsvetkov.constant.Sql.DatabaseSchemaCreator;
import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.util.DbTableCreator;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBTableCreatorImpl implements DbTableCreator {

  private static final DBTableCreatorImpl instance = new DBTableCreatorImpl();

  private DBTableCreatorImpl() {}

  @Override
  public void createTables() throws SQLException {
   Connection conn = ConnectionManager.getConnection().orElseThrow(SQLException::new);
        Statement statement = conn.createStatement();

      statement.executeUpdate(DatabaseSchemaCreator.CREATE_DATABASE_HYPER_REPORTS);
      statement.executeUpdate(DatabaseSchemaCreator.CREATE_TABLE_COMPANY);
      statement.executeUpdate(DatabaseSchemaCreator.CREATE_TABLE_STORE);
      statement.executeUpdate(DatabaseSchemaCreator.CREATE_TABLE_CARD);
      statement.executeUpdate(DatabaseSchemaCreator.CREATE_TABLE_CUSTOMER);
      statement.executeUpdate(DatabaseSchemaCreator.CREATE_TABLE_RECEIPT);
      statement.executeUpdate(DatabaseSchemaCreator.CREATE_TABLE_INVOICE);
      statement.executeUpdate(DatabaseSchemaCreator.CREATE_TABLE_LAST_REPORT_DATES_BY_COMPANY);
  }

  public static DBTableCreatorImpl getInstance() {
    return instance;
  }
}
