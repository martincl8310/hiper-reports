package com.tsvetkov.configuration;

import com.tsvetkov.constant.AppConstants;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

public class DirectoryProperties {

  private static final Properties properties = new Properties();
  private static final DirectoryProperties instance = new DirectoryProperties();

  private DirectoryProperties() {
    FileInputStream file;
    try {
      file = new FileInputStream(AppConstants.DIRECTORY_PROPERTIES);
      properties.load(file);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String getInImportDirectory(){
    return Optional.of(properties.getProperty("importDirectory")).orElseThrow(IllegalStateException::new);
  }

  public String getExportedDirectory(){
    if (properties.getProperty("exportDirectory") == null){
      throw new NullPointerException("Export directory is not set.");
    }else {
      return  properties.getProperty("exportDirectory");
    }

  }

  public static DirectoryProperties getInstance(){
    return instance;
  }
}
