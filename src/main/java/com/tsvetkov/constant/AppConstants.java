package com.tsvetkov.constant;

public class AppConstants {
    public static final String DIRECTORY_PROPERTIES =
            "/home/martin/Desktop/dataDirectory.properties";
    public static final String[] MONTHS_OF_THE_YEAR = {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
    };
}
