package com.tsvetkov.constant.Sql;

public final class DatabaseSchemaCreator {
  private DatabaseSchemaCreator() {}

  public static final String CREATE_TABLE_COMPANY;
  public static final String CREATE_TABLE_STORE;
  public static final String CREATE_TABLE_INVOICE;
  public static final String CREATE_TABLE_RECEIPT;
  public static final String CREATE_TABLE_CUSTOMER;
  public static final String CREATE_TABLE_CARD;
  public static final String CREATE_TABLE_LAST_REPORT_DATES_BY_COMPANY;
  public static final String PERSIST_CARD_DETAILS;
  public static final String PERSIST_COMPANIES;
  public static final String PERSIST_CUSTOMER;
  public static final String PERSIST_INVOICE;
  public static final String PERSIST_RECEIPT;
  public static final String PERSIST_STORE;
  public static final String PERSIST_LAST_REPORT_DAY_BY_COMPANY;
  public static final String COMPANY_NAMES_WITH_CURRENT_DAY_REPORT;
  public static final String CREATE_DATABASE_HYPER_REPORTS;

  public static final String COMPANY = "company";
  public static final String STORE = "store";
  public static final String INVOICE = "invoice";
  public static final String RECEIPT = "receipt";
  public static final String CUSTOMER = "customer";
  public static final String CARD_DETAILS = "card";
  public static final String LAST_REPORT_DATES_BY_COMPANY = "last_report_dates_by_company";
  public static final String HYPER_REPORTS = "hiper_reports_db";

  static {
    CREATE_DATABASE_HYPER_REPORTS =
        String.format("CREATE DATABASE IF NOT EXISTS %s", HYPER_REPORTS);
    CREATE_TABLE_COMPANY =
        String.format(
            ""
                + "CREATE TABLE IF NOT EXISTS `%s` (\n"
                + "\tid INT PRIMARY KEY AUTO_INCREMENT, \n"
                + "\taddress VARCHAR(200) NOT NULL,\n"
                + "\t`name` VARCHAR(100) NOT NULL unique ,\n"
                + "\t`uuid` VARCHAR(200) NOT NULL UNIQUE\n"
                + ")",
            COMPANY);

    CREATE_TABLE_STORE =
        String.format(
            "CREATE TABLE IF NOT EXISTS %s (\n"
                + "id INT PRIMARY KEY AUTO_INCREMENT,\n"
                + " `name` VARCHAR(100) NOT NULL unique,\n"
                + " address VARCHAR(200) NOT NULL,\n"
                + " company_id INT, \n"
                + " CONSTRAINT FK_store_company FOREIGN KEY(company_id) REFERENCES %s(id));",
            STORE, COMPANY);

    CREATE_TABLE_CARD =
        String.format(
            "CREATE TABLE IF NOT EXISTS %s(\n"
                + "id INT PRIMARY KEY AUTO_INCREMENT,\n"
                + "card_type varchar(100) ,\n"
                + "`number` VARCHAR(100) UNIQUE, \n"
                + "contactless TINYINT(1));",
            CARD_DETAILS);
    CREATE_TABLE_CUSTOMER =
        String.format(
            "CREATE TABLE IF NOT EXISTS %s(\n"
                + "id INT PRIMARY KEY AUTO_INCREMENT,\n"
                + "`name` VARCHAR(50) ,\n"
                + "address VARCHAR(200) ,\n"
                + "`uuid` VARCHAR(200) UNIQUE);",
            CUSTOMER);

    CREATE_TABLE_INVOICE =
        String.format(
            "CREATE TABLE IF NOT EXISTS %s(\n"
                + "id INT PRIMARY KEY AUTO_INCREMENT,\n"
                + "`total` DECIMAL(15,2),\n"
                + "local_date DATE,\n"
                + "payment VARCHAR(20),\n"
                + "store_id INT,\n"
                + "customer_id INT,\n"
                + "card_id INT,\n"
                + "CONSTRAINT FK_invoice_store FOREIGN KEY(store_id) REFERENCES %s(id),\n"
                + "CONSTRAINT FK_invoice_customer FOREIGN KEY(customer_id) REFERENCES %s(id),\n"
                + "CONSTRAINT FK_invoice_card_details FOREIGN KEY(card_id) REFERENCES %s(id));",
            INVOICE, STORE, CUSTOMER, CARD_DETAILS);
    CREATE_TABLE_RECEIPT =
        String.format(
            "CREATE TABLE IF NOT EXISTS %s (\n"
                + "id INT PRIMARY KEY AUTO_INCREMENT,\n"
                + "total DECIMAL(7,2) ,\n"
                + "local_date TIMESTAMP ,\n"
                + "payment VARCHAR(20) ,\n"
                + "store_id INT,\n"
                + "card_id INT,\n"
                + "CONSTRAINT FK_receipt_store FOREIGN KEY(store_id) REFERENCES %s(id),\n"
                + "    CONSTRAINT FK_receipt_card_id FOREIGN KEY(card_id) REFERENCES %s(id));",
            RECEIPT, STORE, CARD_DETAILS);

    CREATE_TABLE_LAST_REPORT_DATES_BY_COMPANY =
        String.format(
            " CREATE TABLE IF NOT EXISTS %s(\n"
                + "id INT PRIMARY KEY auto_increment,\n"
                + "local_date date,\n"
                + "company_name varchar(50) unique);",
            LAST_REPORT_DATES_BY_COMPANY);
    PERSIST_CARD_DETAILS =
        String.format(
            "INSERT INTO %s(`card_type`, `number`, `contactless`) " + "VALUES(?, ?, ?)",
            CARD_DETAILS);
    PERSIST_COMPANIES =
        String.format("INSERT INTO %s(`name`, `address`, `uuid`) VALUES(?, ?, ?)", COMPANY);
    PERSIST_CUSTOMER =
        String.format("INSERT INTO %s(`name`, `address`, `uuid`) VALUES(?, ?, ?)", CUSTOMER);
    PERSIST_INVOICE =
        String.format(
            "INSERT INTO %s(`total`, `local_date`, `local_time`, `payment`, `store_id`, `customer_id`, `card_id`)"
                + " VALUES(?, ?, ?, ?, ?,?,?)",
            INVOICE);
    PERSIST_RECEIPT =
        String.format(
            "INSERT INTO %s(`total`, `local_date`, `local_time`, `payment`, `store_id`, `card_id`) VALUES(?, ?, ?, ?, ?,?)",
            RECEIPT);
    PERSIST_STORE =
        String.format("INSERT INTO %s(`name`, `address`, `company_id`) VALUES(?, ?, ?)", STORE);
    PERSIST_LAST_REPORT_DAY_BY_COMPANY =
        String.format(
            "INSERT INTO %s(`local_date`, `company_name`) VALUES(?, ?)",
            CREATE_TABLE_LAST_REPORT_DATES_BY_COMPANY);
    COMPANY_NAMES_WITH_CURRENT_DAY_REPORT =
        String.format("SELECT company_name FROM %s", LAST_REPORT_DATES_BY_COMPANY);
  }
}
