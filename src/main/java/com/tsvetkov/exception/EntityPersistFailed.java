package com.tsvetkov.exception;

public class EntityPersistFailed extends Exception {

  public EntityPersistFailed(String message) {
    super(message);
  }
}
