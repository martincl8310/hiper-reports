package com.tsvetkov;

import com.tsvetkov.jcommander.CommandLineParser;
import com.tsvetkov.jcommander.CommandlineParameters;
import com.tsvetkov.util.impl.DBTableCreatorImpl;
import org.apache.commons.configuration.ConfigurationException;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HyperReports {

  static final CommandlineParameters parameters = new CommandlineParameters();

  public static void main(String[] args) throws SQLException {

    try {
      CommandLineParser argumentsParser = new CommandLineParser();
      argumentsParser.setParameters(parameters);
      argumentsParser.parse(args);
    } catch (ConfigurationException e) {
      Logger.getLogger(DBTableCreatorImpl.class.getName()).log(Level.SEVERE, e.getMessage(), e);
    }
  }
}
