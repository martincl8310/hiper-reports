package com.tsvetkov.specification;

import com.tsvetkov.util.sqlutils.utils.QueryInfo;

import java.util.HashMap;
import java.util.Map;

public class FindReceiptByDate implements Specification {
    private int storeId;
    private int month;
    private int year;

    public FindReceiptByDate(int storeId, int month, int year) {
        this.storeId = storeId;
        this.month = month;
        this.year = year;
    }

    @Override
    public QueryInfo toQueryInfo() {
        QueryInfo queryInfo = new QueryInfo();
        String query =
          "SELECT r.* from receipt as r\n" +
                  "join store as s on r.store_id=s.id\n" +
                  "where Month(r.local_date) = ?\n" +
                  "and year(r.local_date) = ? and s.id = ?\n" +
                  "group by r.id order by r.id";

        Map<Integer, Object> placeholders = new HashMap<>();
        placeholders.put(1, storeId);
        placeholders.put(2, month);
        placeholders.put(3, year);
        queryInfo.setQuery(query);
        queryInfo.setPlaceholders(placeholders);
        return queryInfo;
    }
}
