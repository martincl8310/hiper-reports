package com.tsvetkov.specification;

import com.tsvetkov.util.sqlutils.utils.QueryInfo;

public interface Specification {
    QueryInfo toQueryInfo();
}
