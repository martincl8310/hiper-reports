package com.tsvetkov.template.impl;

import com.tsvetkov.annotation.Column;
import com.tsvetkov.model.entity.BaseEntity;
import com.tsvetkov.repository.impl.JdbcRepository;
import com.tsvetkov.template.Template;
import com.tsvetkov.util.impl.ReflectionUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TemplateImpl<T extends BaseEntity> implements Template<T> {
  private final Class<T> tClass;
  private static final String ID = "id";

  public TemplateImpl(Class<T> tClass) {
    this.tClass = tClass;
  }

  @Override
  public Optional<T> mapRowToEntity(
      Connection connection, String query, Map<Integer, Object> placeholders) {

    try (PreparedStatement statement = connection.prepareStatement(query); ) {

      for (Map.Entry<Integer, Object> entry : placeholders.entrySet()) {
        statement.setObject(entry.getKey(), entry.getValue());
      }
      T entity = tClass.getDeclaredConstructor().newInstance();
      ResultSet resultSet = statement.executeQuery();
      Field[] declaredFields = tClass.getDeclaredFields();

      while (resultSet.next()) {
        for (Field field : declaredFields) {
          field.setAccessible(true);
          if (field.getGenericType().getTypeName().contains("List")) {
            continue;
          }

          mapColumnToEntityField(entity, resultSet, field);
        }
        entity.setId(resultSet.getInt(ID));
      }
      if (entity.getId() != null) {
        return Optional.of(entity);
      }
    } catch (InstantiationException
        | InvocationTargetException
        | NoSuchMethodException
        | IllegalAccessException
        | SQLException e) {
      Logger.getLogger(JdbcRepository.class.getName()).log(Level.SEVERE, e.getMessage(), e);
    }
    return Optional.empty();
  }

  @Override
  public List<T> mapRowToAllEntity(
      Connection connection, String query, Map<Integer, Object> placeholders) {
    try (PreparedStatement statement = connection.prepareStatement(query); ) {
      List<T> result = new ArrayList<>();
      for (Map.Entry<Integer, Object> entry : placeholders.entrySet()) {
        statement.setObject(entry.getKey(), entry.getValue());
      }
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        T entity = tClass.getDeclaredConstructor().newInstance();
        Field[] declaredFields = tClass.getDeclaredFields();
        for (Field field : declaredFields) {
          field.setAccessible(true);
          if (field.getGenericType().getTypeName().contains("List")) {
            continue;
          }

          mapColumnToEntityField(entity, resultSet, field);
        }
        entity.setId(resultSet.getInt(ID));
        result.add(entity);
      }
      if (!result.isEmpty()) {
        return result;
      }
    } catch (InstantiationException
        | InvocationTargetException
        | NoSuchMethodException
        | IllegalAccessException
        | SQLException e) {
      Logger.getLogger(JdbcRepository.class.getName()).log(Level.SEVERE, e.getMessage(), e);
    }

    return Collections.emptyList();
  }

  public void mapColumnToEntityField(T entity, ResultSet resultSet, Field field)
      throws IllegalAccessException, SQLException {
    if (field.isAnnotationPresent(Column.class)) {
      String fieldName = field.getName();
      fieldName = nameConversion(fieldName);
      if (field.getType().equals(Boolean.class)) {
        field.set(entity, resultSet.getBoolean(fieldName));
      } else if (field.getType().equals(Integer.class)) {
        field.set(entity, resultSet.getInt(fieldName));
      } else {
        field.set(entity, resultSet.getObject(fieldName));
      }
    }
  }

  @Override
  public Map<String, Object> convertEntityToRow(T entity) {
    Map<String, Object> data = new LinkedHashMap<>();

    List<Field> declaredFields = ReflectionUtil.getClassFields(tClass, new ArrayList<>());
    for (Field field : declaredFields) {

      field.setAccessible(true);
      if (field.isAnnotationPresent(Column.class)) {
        try {
          String fieldName = field.getName();
          Object value = field.get(entity);

          if (!field.getType().isInstance(BaseEntity.class)
              && !ID.equals(fieldName)
              && !field.getGenericType().getTypeName().contains("List")) {
            data.put(nameConversion(fieldName), value);
          } else if (field.getType().isInstance(BaseEntity.class)) {
            data.put(nameConversion(fieldName.toLowerCase()) + "_id", ((BaseEntity) value).getId());
          }
        } catch (IllegalAccessException e) {
          Logger.getLogger(JdbcRepository.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
      }
    }
    return data;
  }

  private String nameConversion(String fieldName) {
    char[] chars = fieldName.toCharArray();
    StringBuilder sb = new StringBuilder(0);
    sb.append(chars[0]);

    for (int i = 1; i < chars.length; i++) {
      String letter = Character.toString(chars[i]);
      if (letter.equals(letter.toUpperCase())) {
        sb.append('_');
      }
      sb.append(letter.toLowerCase());
    }
    return sb.toString();
  }
}
