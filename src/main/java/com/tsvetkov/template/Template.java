package com.tsvetkov.template;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface Template<T> {

     Optional<T>mapRowToEntity(
            Connection connection, String query, Map<Integer, Object> placeholders);

    Map<String, Object> convertEntityToRow(T entity);

    List<T> mapRowToAllEntity(Connection connection, String query, Map<Integer, Object> placeholders);
}
