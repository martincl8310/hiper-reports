package com.tsvetkov.service;

import com.tsvetkov.model.dto.CompanyDto;
import com.tsvetkov.model.entity.Company;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public interface CompanyService {

  CompanyDto persist(CompanyDto companyDto) throws IllegalAccessException, SQLException;

  Optional<Company> findByCompanyName(String columnName, String columnValue, Connection connection) throws SQLException;
}
