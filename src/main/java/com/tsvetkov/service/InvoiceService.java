package com.tsvetkov.service;

import com.tsvetkov.exception.EntityPersistFailed;
import com.tsvetkov.model.entity.Invoice;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface InvoiceService {

  Optional<Invoice> persist(Invoice invoice, Connection conn)
      throws SQLException, EntityPersistFailed;

  List<Invoice> findByDateAndStoreId(int year, int month, int storeId);

  List<Invoice> findByDayOfMonth(int year, int month, int day, int storeId);

  List<Invoice> findByInvoicePaymentAndMonth(int year, int month, String payment, int companyId);

  List<Invoice> findInvoiceByPaymentPerDay(int year, int month, int day, String payment, int companyId);
}
