package com.tsvetkov.service;

import com.tsvetkov.exception.EntityPersistFailed;
import com.tsvetkov.model.entity.Store;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface StoreService {
  Optional<Store> findStoreByName(String columnName, String storeName, Connection connection)
          throws SQLException;

  Optional<Store> persist(Store store, Connection con) throws SQLException, EntityPersistFailed;

  List<Store> findStoreByCompanyId(int id, Connection connection);
}
