package com.tsvetkov.service;

import com.tsvetkov.exception.EntityPersistFailed;
import com.tsvetkov.model.entity.Receipt;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface ReceiptService {
  Optional<Receipt> persistReceipt(Receipt receipt, Connection con)
      throws SQLException, EntityPersistFailed;

  List<Receipt> findByDateAndStoreId(int year, int month, int storeId);

  List<Receipt> findReceiptByDayOfMonth(int year, int month,int day, int storeId);

  List<Receipt> findReceiptByPaymentAndMonth(int year, int month, String payment, int companyId);

  List<Receipt> findReceiptByPaymentPerDayAndMonth(int year, int month, int day, String payment, int companyId);
}
