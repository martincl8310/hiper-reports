package com.tsvetkov.service.impl;

import com.tsvetkov.model.entity.Customer;
import com.tsvetkov.repository.Repository;
import com.tsvetkov.service.CustomerService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public class CustomerServiceImpl implements CustomerService {

    private final Repository<Customer> customerRepository;

    public CustomerServiceImpl(Repository<Customer> customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Optional<Customer> persist(Customer customer, Connection con) {
        return this.customerRepository.persist(customer, con);
    }

    @Override
    public Optional<Customer> findByField(String columnName, String columnValue, Connection connection) throws SQLException {
        return this.customerRepository.findByField(columnName,columnValue, connection);
    }
}
