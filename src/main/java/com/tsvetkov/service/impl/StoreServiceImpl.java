package com.tsvetkov.service.impl;

import com.tsvetkov.model.entity.Store;
import com.tsvetkov.repository.Repository;
import com.tsvetkov.service.StoreService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class StoreServiceImpl implements StoreService {

  private final Repository<Store> storeRepository;

  public StoreServiceImpl(Repository<Store> storeRepository) {
    this.storeRepository = storeRepository;
  }

  @Override
  public Optional<Store> findStoreByName(String columnName, String storeName, Connection con) throws SQLException {
    return this.storeRepository.findByField(columnName, storeName, con);
  }

  @Override
  public Optional<Store> persist(Store store, Connection con) {
    return this.storeRepository.persist(store, con);
  }

  @Override
  public List<Store> findStoreByCompanyId(int id, Connection connection) {
    return this.storeRepository.findAllById(id, connection);
  }
}
