package com.tsvetkov.service.impl;

import com.tsvetkov.model.entity.Card;
import com.tsvetkov.repository.Repository;
import com.tsvetkov.service.CardService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public class CardServiceImpl implements CardService {

  private final Repository<Card> cardRepository;

  public CardServiceImpl(Repository<Card> cardRepository) {
    this.cardRepository = cardRepository;
  }

  @Override
  public Optional<Card> persist(Card card, Connection connection) {
    return this.cardRepository.persist(card, connection);
  }

  @Override
  public Optional<Card> findCardByNumber(String columnName, String cardNumber, Connection connection) throws SQLException {
    return this.cardRepository.findByField(columnName, cardNumber, connection);
  }
}
