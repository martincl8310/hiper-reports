package com.tsvetkov.service.impl;

import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.repository.impl.InvoiceRepository;
import com.tsvetkov.service.InvoiceService;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public class InvoiceServiceImpl implements InvoiceService {

  private final InvoiceRepository invoiceRepository;

  public InvoiceServiceImpl(InvoiceRepository invoiceRepository) {
    this.invoiceRepository = invoiceRepository;
  }

  @Override
  public Optional<Invoice> persist(Invoice invoice, Connection con) {
    return this.invoiceRepository.persist(invoice, con);
  }

  @Override
  public List<Invoice> findByDateAndStoreId(int year, int month, int storeId) {
    return this.invoiceRepository.findByDateAndStoreId(year, month, storeId);
  }

  @Override
  public List<Invoice> findByDayOfMonth(int year, int month, int day, int storeId) {
    return this.invoiceRepository.findByDayOfMonth(year, month, day, storeId);
  }

  @Override
  public List<Invoice> findByInvoicePaymentAndMonth(
      int year, int month, String payment, int companyId) {
    return this.invoiceRepository.findByPaymentPerMonth(year, month, payment, companyId);
  }

  @Override
  public List<Invoice> findInvoiceByPaymentPerDay(
      int year, int month, int day, String payment, int companyId) {
    return this.invoiceRepository.findByPaymentPerDay(year, month, day, payment, companyId);
  }
}
