package com.tsvetkov.service.impl;

import com.tsvetkov.model.entity.Receipt;
import com.tsvetkov.repository.impl.ReceiptRepository;
import com.tsvetkov.service.ReceiptService;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public class ReceiptServiceImpl implements ReceiptService {

  private final ReceiptRepository receiptRepository;

  public ReceiptServiceImpl(ReceiptRepository receiptRepository) {
    this.receiptRepository = receiptRepository;
  }

  @Override
  public Optional<Receipt> persistReceipt(Receipt receipt, Connection con) {
    return this.receiptRepository.persist(receipt, con);
  }

  @Override
  public List<Receipt> findByDateAndStoreId(int year, int month, int storeId) {
    return this.receiptRepository.findByDateAndStoreId(year, month, storeId);
  }

  @Override
  public List<Receipt> findReceiptByDayOfMonth(int year, int month, int day, int storeId) {
    return this.receiptRepository.findByDayOfMonth(year, month, day, storeId);
  }

  @Override
  public List<Receipt> findReceiptByPaymentAndMonth(
      int year, int month, String payment, int storeId) {
    return this.receiptRepository.findByPayment(year, month, payment, storeId);
  }

  @Override
  public List<Receipt> findReceiptByPaymentPerDayAndMonth(int year, int month, int day, String payment, int companyId) {
    return this.receiptRepository.findByPaymentPerDayAndMonth(year, month, day, payment, companyId);
  }
}
