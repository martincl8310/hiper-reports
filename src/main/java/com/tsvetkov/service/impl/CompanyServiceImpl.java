package com.tsvetkov.service.impl;

import com.tsvetkov.exception.EntityPersistFailed;
import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.mapper.ModelMapperConfig;
import com.tsvetkov.model.dto.CompanyDto;
import com.tsvetkov.model.entity.*;
import com.tsvetkov.repository.Repository;
import com.tsvetkov.service.*;
import org.modelmapper.ModelMapper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CompanyServiceImpl implements CompanyService {
  private Repository<Company> companyRepository;
  private StoreService storeService;
  private InvoiceService invoiceService;
  private ReceiptService receiptService;
  private ModelMapper modelMapper;
  private CardService cardService;
  private CustomerService customerService;

  public CompanyServiceImpl(Repository<Company> companyRepositor) {
      this.companyRepository = companyRepositor;
  }

  public CompanyServiceImpl(
      Repository<Company> companyRepository,
      StoreService storeService,
      InvoiceService invoiceService,
      ReceiptService receiptService,
      CustomerService customerService,
      CardService cardService) {
    this.companyRepository = companyRepository;
    this.invoiceService = invoiceService;
    this.storeService = storeService;
    this.receiptService = receiptService;
    this.customerService = customerService;
    this.cardService = cardService;
    this.modelMapper = ModelMapperConfig.getInstance();
  }

  @Override
  public CompanyDto persist(CompanyDto companyDto) throws SQLException {
    Company entity = this.modelMapper.map(companyDto, Company.class);
    Connection con = ConnectionManager.getConnection().orElseThrow(SQLException::new);

    try (con) {
      Optional<Company> companyByName = findByCompanyName("name", entity.getName(), con);
      if (companyByName.isPresent()) {
        entity.setId(companyByName.get().getId());
        entity.setUuid(companyByName.get().getUuid());
      } else {
        this.companyRepository.persist(entity, con);
      }

      List<Store> stores = entity.getStores();
      for (Store store : stores) {
        store.setCompanyId(entity.getId());
        Optional<Store> storeByName =
            this.storeService.findStoreByName("name", store.getName(), con);
        if (storeByName.isPresent()) {
          store.setId(storeByName.get().getId());
        } else {
          this.storeService.persist(store, con);
        }
        List<Invoice> invoices = store.getInvoices();
        List<Receipt> receipts = store.getReceipts();

        for (Invoice invoice : invoices) {
          invoice.setStoreId(store.getId());
          Card cardDetails = invoice.getCardDetails();
          if (cardDetails != null) {
            Optional<Card> cardByNumber =
                this.cardService.findCardByNumber("number", cardDetails.getNumber(), con);
            if (cardByNumber.isPresent()) {
              invoice.setCardId(cardByNumber.get().getId());
            } else {
              Card savedCard =
                  this.cardService
                      .persist(cardDetails, con)
                      .orElseThrow(() -> new EntityPersistFailed("Fail to persist Card entity"));
              invoice.setCardId(savedCard.getId());
            }
          }
          Customer customer = invoice.getCustomer();
          if (customer != null) {
            Optional<Customer> customerByUuid =
                this.customerService.findByField("uuid", customer.getUuid(), con);
            if (customerByUuid.isPresent()) {
              customer.setId(customerByUuid.get().getId());
              invoice.setCustomerId(customerByUuid.get().getId());
            } else {
              Customer savedCustomer =
                  this.customerService
                      .persist(customer, con)
                      .orElseThrow(() -> new EntityPersistFailed("Fail to persist an entity"));
              invoice.setCustomerId(savedCustomer.getId());
            }
          }
          this.invoiceService.persist(invoice, con);
        }

        for (Receipt receipt : receipts) {
          receipt.setStoreId(store.getId());
          Card cardDetails = receipt.getCardDetails();
          if (cardDetails != null) {
            Optional<Card> cardByNumber =
                this.cardService.findCardByNumber("number", cardDetails.getNumber(), con);
            if (cardByNumber.isPresent()) {
              receipt.setCardId(cardByNumber.get().getId());
            } else {
              Card savedCard =
                  this.cardService
                      .persist(cardDetails, con)
                      .orElseThrow(() -> new EntityPersistFailed("Fail to persist an entity"));
              receipt.setCardId(savedCard.getId());
            }
          }
          this.receiptService.persistReceipt(receipt, con);
        }
      }
    } catch (EntityPersistFailed e) {
      Logger.getLogger(CompanyServiceImpl.class.getName()).log(Level.SEVERE, e.getMessage(), e);
    }
    return this.modelMapper.map(entity, CompanyDto.class);
  }

  @Override
  public Optional<Company> findByCompanyName(
      String columnName, String columnValue, Connection connection) throws SQLException {
    return this.companyRepository.findByField(columnName, columnValue, connection);
  }
}
