package com.tsvetkov.service.impl;

import com.tsvetkov.model.dto.CompanyDto;
import com.tsvetkov.model.entity.*;
import com.tsvetkov.repository.impl.InvoiceRepository;
import com.tsvetkov.repository.impl.JdbcRepository;
import com.tsvetkov.repository.impl.ReceiptRepository;
import com.tsvetkov.service.CompanyService;
import com.tsvetkov.util.FileLister;
import com.tsvetkov.util.XmlDeserializer;
import com.tsvetkov.util.impl.FileListerImpl;
import com.tsvetkov.util.impl.XmlDeserializerImpl;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.List;

public class CompanyDataProcessor {
  private final CompanyService companyService =
      new CompanyServiceImpl(
          new JdbcRepository<>(Company.class),
          new StoreServiceImpl(new JdbcRepository<>(Store.class)),
          new InvoiceServiceImpl(new InvoiceRepository(Invoice.class)),
          new ReceiptServiceImpl(new ReceiptRepository(Receipt.class)),
          new CustomerServiceImpl(new JdbcRepository<>(Customer.class)),
          new CardServiceImpl(new JdbcRepository<>(Card.class)));
  private final XmlDeserializer parser;
  private final FileLister fileLister;


  public CompanyDataProcessor() {
    this.parser = XmlDeserializerImpl.getInstance();
    this.fileLister = FileListerImpl.getInstance();
  }

  public void processData(String importDirectoryPath) {

    CompanyDto companyDto;
    try {

      List<Path> filePaths = fileLister.getFilePaths(importDirectoryPath);
      if (filePaths.isEmpty()){
        System.out.println("No new data!");
        System.exit(0);
        return;
      }
      for (Path filePath : filePaths) {
        companyDto = parser.unmarshallFromXml(CompanyDto.class, filePath.toString());
        companyService.persist(companyDto);
      }
    } catch (IllegalAccessException | IOException | JAXBException | SQLException e) {
      System.exit(0);
    }
  }

}
