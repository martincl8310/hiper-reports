package com.tsvetkov.service;

import com.tsvetkov.exception.EntityPersistFailed;
import com.tsvetkov.model.entity.Customer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public interface CustomerService {
  Optional<Customer> persist(Customer customer, Connection con)
      throws SQLException, EntityPersistFailed;

  Optional<Customer> findByField(String columnName, String columnValue, Connection connection) throws SQLException;
}
