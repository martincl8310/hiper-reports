package com.tsvetkov.service;

import com.tsvetkov.exception.EntityPersistFailed;
import com.tsvetkov.model.entity.Card;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public interface CardService {
  Optional<Card> persist(Card cardDetails, Connection con) throws SQLException, EntityPersistFailed;

  Optional<Card> findCardByNumber(String columnName, String cardNumber, Connection connection) throws SQLException;
}
