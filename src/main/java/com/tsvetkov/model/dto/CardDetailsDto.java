package com.tsvetkov.model.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "carddetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardDetailsDto {

  @XmlElement(name = "cardtype")
  private String cardType;

  @XmlElement(name = "number")
  private String number;

  @XmlElement(name = "contactless")
  private Boolean contactless;

  public String getCardType() {
    return cardType;
  }

  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Boolean getContactless() {
    return contactless;
  }

  public void setContactless(Boolean contactless) {
    this.contactless = contactless;
  }

  @Override
  public String toString() {
    return "CardDetails{" +
            "cardType='" + cardType + '\'' +
            ", number=" + number +
            ", contactless=" + contactless +
            '}';
  }
}
