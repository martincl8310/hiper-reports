package com.tsvetkov.model.dto;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "company")
@XmlAccessorType(XmlAccessType.FIELD)
public class CompanyDto {

  @XmlAttribute
  private String name;

  @XmlAttribute
  private String address;

  @XmlAttribute
  private String uuid;

  @XmlElementWrapper(name = "stores")
  @XmlElement(name = "store")
  private List<StoreDto> stores;


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public List<StoreDto> getStores() {
    return stores;
  }

  public void setStores(List<StoreDto> stores) {
    this.stores = stores;
  }

  @Override
  public String toString() {
    return "Company{" +
            "name='" + name + '\'' +
            ", address='" + address + '\'' +
            ", uuid='" + uuid + '\'' +
            ", stores=" + stores +
            '}';
  }
}
