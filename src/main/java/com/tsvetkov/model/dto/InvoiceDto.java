package com.tsvetkov.model.dto;

import com.tsvetkov.util.impl.LocalDateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@XmlRootElement(name = "invoice")
@XmlAccessorType(XmlAccessType.FIELD)
public class InvoiceDto {

  @XmlElement(name = "total")
  private BigDecimal total;

  @XmlElement(name = "datetime")
  @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
  private LocalDateTime dateTime;

  @XmlElement(name = "payment")
  private String payment;

  @XmlElement(name = "carddetails")
  private CardDetailsDto carddetails;

  @XmlElement(name = "customer")
  private CustomerDto customer;

  public BigDecimal getTotal() {
    return total;
  }

  public void setTotal(BigDecimal total) {
    this.total = total;
  }

  public LocalDateTime getDateTime() {
    return dateTime;
  }

  public void setDateTime(LocalDateTime dateTime) {
    this.dateTime = dateTime;
  }

  public String getPayment() {
    return payment;
  }

  public void setPayment(String payment) {
    this.payment = payment;
  }

  public CustomerDto getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerDto customer) {
    this.customer = customer;
  }

  public CardDetailsDto getCarddetails() {
    return carddetails;
  }

  public void setCarddetails(CardDetailsDto carddetails) {
    this.carddetails = carddetails;
  }

  @Override
  public String toString() {
    return "InvoiceDto{" +
            "total=" + total +
            ", dateTime=" + dateTime +
            ", payment='" + payment + '\'' +
            ", carddetails=" + carddetails +
            ", customer=" + customer +
            '}';
  }
}
