package com.tsvetkov.model.dto;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "store")
@XmlAccessorType(XmlAccessType.FIELD)
public class StoreDto {
  private CompanyDto companyDto;

  @XmlAttribute(name = "name")
  private String name;

  @XmlAttribute(name = "address")
  private String address;

  @XmlElementWrapper(name = "receipts")
  @XmlElement(name = "receipt")
  private List<ReceiptDto> receipts;

  @XmlElementWrapper(name = "invoices")
  @XmlElement(name = "invoice")
  private List<InvoiceDto> invoices;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public List<ReceiptDto> getReceipts() {
    return receipts;
  }

  public void setReceipts(List<ReceiptDto> receipts) {
    this.receipts = receipts;
  }

  public List<InvoiceDto> getInvoices() {
    return invoices;
  }

  public void setInvoices(List<InvoiceDto> invoices) {
    this.invoices = invoices;
  }

  public CompanyDto getCompanyDto() {
    return companyDto;
  }

  public void setCompanyDto(CompanyDto companyDto) {
    this.companyDto = companyDto;
  }

  @Override
  public String toString() {
    return "Store{" +
            "name='" + name + '\'' +
            ", address='" + address + '\'' +
            ", receipts=" + receipts +
            ", invoices=" + invoices +
            '}';
  }
}
