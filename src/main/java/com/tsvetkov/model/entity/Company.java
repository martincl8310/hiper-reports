package com.tsvetkov.model.entity;

import com.tsvetkov.annotation.Column;

import java.util.List;

public class Company extends BaseEntity {

  @Column(name = "name")
  private String name;

  @Column(name = "address")
  private String address;

  @Column(name = "uuid")
  private String uuid;

  private List<Store> stores;

  public Company() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public List<Store> getStores() {
    return stores;
  }

  public void setStores(List<Store> stores) {
    this.stores = stores;
  }
}
