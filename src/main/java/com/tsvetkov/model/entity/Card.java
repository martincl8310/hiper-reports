package com.tsvetkov.model.entity;

import com.tsvetkov.annotation.Column;

public class Card extends BaseEntity {

  @Column(name = "card_type")
  private String cardType;

  @Column(name = "number")
  private String number;

  @Column(name = "contactless")
  private Boolean contactless;

  public Card() {}

  public String getCardType() {
    return cardType;
  }

  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Boolean getContactless() {
    return contactless;
  }

  public void setContactless(Boolean contactless) {
    this.contactless = contactless;
  }
}
