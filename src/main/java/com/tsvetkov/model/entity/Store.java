package com.tsvetkov.model.entity;

import com.tsvetkov.annotation.Column;

import java.util.List;

public class Store extends BaseEntity {

  @Column(name = "name")
  private String name;

  @Column(name = "address")
  private String address;

  @Column(name = "company_id")
  private Integer companyId;

  private List<Receipt> receipts;

  private List<Invoice> invoices;

  public Store() {}
  public Store(int id, String name, String address){
    this.setId(id);
    this.setName(name);
    this.setAddress(address);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public List<Receipt> getReceipts() {
    return receipts;
  }

  public void setReceipts(List<Receipt> receipts) {
    this.receipts = receipts;
  }

  public List<Invoice> getInvoices() {
    return invoices;
  }

  public void setInvoices(List<Invoice> invoices) {
    this.invoices = invoices;
  }

  public Integer getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }
}
