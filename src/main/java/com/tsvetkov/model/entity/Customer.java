package com.tsvetkov.model.entity;

import com.tsvetkov.annotation.Column;

public class Customer extends BaseEntity {

  @Column(name = "name")
  private String name;

  @Column(name = "address")
  private String address;

  @Column(name = "uuid")
  private String uuid;

  public Customer() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }
}
