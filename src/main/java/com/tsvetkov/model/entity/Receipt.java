package com.tsvetkov.model.entity;

import com.tsvetkov.annotation.Column;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Receipt extends BaseEntity {
  @Column(name = "total")
  private BigDecimal total;

  @Column(name = "local_date")
  private LocalDateTime localDate;

  @Column(name = "payment")
  private String payment;

  @Column(name = "store_id")
  private Integer storeId;

  @Column(name = "card_id")
  private Integer cardId;

  private Card cardDetails;

  private Store store;

  public Receipt() {}

  public Receipt(BigDecimal total){
    this.setTotal(total);
  }

  public BigDecimal getTotal() {
    return total;
  }

  public void setTotal(BigDecimal total) {
    this.total = total;
  }

  public String getPayment() {
    return payment;
  }

  public void setPayment(String payment) {
    this.payment = payment;
  }

  public Card getCardDetails() {
    return cardDetails;
  }

  public void setCardDetails(Card cardDetails) {
    this.cardDetails = cardDetails;
  }

  public Store getStore() {
    return store;
  }

  public void setStore(Store store) {
    this.store = store;
  }

  public LocalDateTime getLocalDate() {
    return localDate;
  }

  public void setLocalDate(LocalDateTime localDate) {
    this.localDate = localDate;
  }

  public Integer getStoreId() {
    return storeId;
  }

  public void setStoreId(Integer storeId) {
    this.storeId = storeId;
  }

  public Integer getCardId() {
    return cardId;
  }

  public void setCardId(Integer cardId) {
    this.cardId = cardId;
  }
}
