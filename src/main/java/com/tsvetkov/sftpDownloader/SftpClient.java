package com.tsvetkov.sftpDownloader;

public interface SftpClient {
    void downloadFiles(String destinationPat) throws Exception;

}
