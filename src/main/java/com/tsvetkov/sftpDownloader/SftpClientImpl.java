package com.tsvetkov.sftpDownloader;

import com.jcraft.jsch.*;
import com.tsvetkov.repository.impl.CurrentDateByCompanyRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SftpClientImpl implements SftpClient {

  private Session session;
  private final SftpClientDetails sftpClientDetails;

  public SftpClientImpl(SftpClientDetails sftpClientDetails) {
    this.sftpClientDetails = sftpClientDetails;
  }

  private void connect() throws JSchException {
    JSch jSch = new JSch();
    this.session =
        jSch.getSession(
            sftpClientDetails.getUsername(),
            this.sftpClientDetails.getHost(),
            this.sftpClientDetails.getPort());
    this.session.setPassword(this.sftpClientDetails.getPassword());

    this.session.setConfig("StrictHostKeyChecking", "no");
    this.session.connect();
  }

  @Override
  public void downloadFiles(String destinationPath) throws Exception {
    if (destinationPath.charAt(destinationPath.length() - 1) != '/') {
      destinationPath = destinationPath + '/';
    }

    try {
      connect();
      Channel channel = session.openChannel("sftp");
      channel.connect();
      ChannelSftp channelSftp = (ChannelSftp) channel;
      channelSftp.cd(this.sftpClientDetails.getSftpDataFolder());
      Vector<ChannelSftp.LsEntry> downloadedFiles = channelSftp.ls("*.xml");

      List<ChannelSftp.LsEntry> sortedXmlFiles =
          downloadedFiles.stream().sorted().collect(Collectors.toList());

      CurrentDateByCompanyRepository repository = new CurrentDateByCompanyRepository();

      for (ChannelSftp.LsEntry xmlFile : sortedXmlFiles) {

        if (xmlFile.getFilename() != null) {

          String companyName =
              getCompanyName(xmlFile.getFilename()).orElseThrow(NullPointerException::new);
          String date = getDate(xmlFile.getFilename()).orElseThrow(NullPointerException::new);

          List<String> companyNames = repository.companyNamesWithCurrentDateReport();

          if (companyNames.contains(companyName)) {
            String currentDateByCompany = repository.lastReportDayByCompany(companyName);
            LocalDate currentDateReport = LocalDate.parse(currentDateByCompany);
            LocalDate newDateReport = LocalDate.parse(date);
            if (currentDateReport.isBefore(newDateReport)) {
              repository.updateCompanyLastReportDate(companyName, String.valueOf(newDateReport));
              channelSftp.get(xmlFile.getFilename(), destinationPath + xmlFile.getFilename());
            }
          } else {
            repository.persistCompanyNameAndNewDate(date, companyName);
            channelSftp.get(xmlFile.getFilename(), destinationPath + xmlFile.getFilename());
          }
        }
      }
    } catch (JSchException e) {
      Logger.getLogger(SftpClient.class.getName()).log(Level.SEVERE, "", e);
      System.exit(0);
    } finally {
      disconnect();
    }
  }

  private Optional<String> getDate(String filename) {
    Pattern pattern = Pattern.compile("([\\d-]+)(-)([a-zA-Z]+)");
    Matcher matcher = pattern.matcher(filename);
    if (matcher.find()) {
      return Optional.of(matcher.group(1));
    }
    return Optional.empty();
  }

  private Optional<String> getCompanyName(String filename) {
    Pattern pattern = Pattern.compile("([\\d-]+)(-)([a-zA-Z]+)");
    Matcher matcher = pattern.matcher(filename);
    if (matcher.find()) {
      return Optional.of(matcher.group(3));
    }
    return Optional.empty();
  }

  private void disconnect() {
    if (this.session != null) {
      this.session.disconnect();
    }
  }
}
