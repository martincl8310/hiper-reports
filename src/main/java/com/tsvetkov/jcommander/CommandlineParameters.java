package com.tsvetkov.jcommander;

import com.beust.jcommander.Parameter;
import com.tsvetkov.jcommander.validation.*;

import java.util.List;

public class CommandlineParameters {

    @Parameter(names = "config",
            description = "Configuring the export and import directories")
    private boolean config;

    @Parameter(names = {"-h", "--help"},
            help = true,
            description = "Display available commands")
    private boolean help;

    @Parameter(names = "--data-dir",
            description = "A directory where .xml files will be saved",
            validateWith = DirectoryValidator.class)
    private String importDir;

    @Parameter(names = "--export-dir",
            description = "A directory where .xlsx files will be stored",
            validateWith = DirectoryValidator.class)
    private String exportDir;

    @Parameter(names = "process",
            description = "Download data from sftp server.")
    private boolean process;

    @Parameter(names = {"report", "r"})
    private boolean report;

    @Parameter(names = {"--company","-c"},
    validateWith = CompanyValidator.class)
    private String company;

    @Parameter(names = {"--month", "-m"},
    validateWith = MonthValidator.class)
    private List<Integer> months;

    @Parameter(names = {"--quarter", "-q"})
    private String quarter;

    @Parameter(names = {"--year", "-y"},
        validateWith = YearValidator.class)
    private int year;

    @Parameter(names = {"--aggregation", "-a"},
    validateWith = AggregationValidator.class)
    private String aggregation;

    @Parameter(names = {"--order", "-o"})
    private String order;

    @Parameter(names="--top")
    private int top;


    public boolean isHelp() {
        return help;
    }

    public void setHelp(boolean help) {
        this.help = help;
    }

    public boolean isConfig() {
        return config;
    }

    public void setConfig(boolean config) {
        this.config = config;
    }

    public String getImportDir() {
        return importDir;
    }

    public void setImportDir(String importDir) {
        this.importDir = importDir;
    }

    public String getExportDir() {
        return exportDir;
    }

    public void setExportDir(String exportDir) {
        this.exportDir = exportDir;
    }

    public boolean isProcess() {
        return process;
    }

    public void setProcess(boolean process) {
        this.process = process;
    }

    public List<Integer> getMonths() {
        return months;
    }

    public void setMonths(List<Integer> months) {
        this.months = months;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public boolean isReport() {
        return report;
    }

    public void setReport(boolean report) {
        this.report = report;
    }

    public String getAggregation() {
        return aggregation;
    }

    public void setAggregation(String aggregation) {
        this.aggregation = aggregation;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }
}
