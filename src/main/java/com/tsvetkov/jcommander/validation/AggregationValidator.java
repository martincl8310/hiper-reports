package com.tsvetkov.jcommander.validation;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;
import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.model.entity.Receipt;
import com.tsvetkov.model.entity.Store;

import java.util.List;

public class AggregationValidator implements IParameterValidator {


    @Override
    public void validate(String name, String value) throws ParameterException {
        List<String> aggregationParameters = List.of(Store.class.getSimpleName().toLowerCase(),
                Invoice.class.getSimpleName().toLowerCase(),
                Receipt.class.getSimpleName().toLowerCase(),
                "payment");

        if (!aggregationParameters.contains(value)){
            String message = String.format("Unknown value %s for aggregation parameter.", value);
            throw new ParameterException(message);
        }
    }
}
