package com.tsvetkov.jcommander.validation;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;
import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.model.entity.Company;
import com.tsvetkov.repository.impl.JdbcRepository;
import com.tsvetkov.service.CompanyService;
import com.tsvetkov.service.impl.CompanyServiceImpl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CompanyValidator implements IParameterValidator {
    CompanyService companyService = new CompanyServiceImpl(new JdbcRepository<>(Company.class));

    @Override
    public void validate(String name, String value) throws ParameterException {

        try(Connection connection = ConnectionManager.getConnection().orElseThrow();) {

             Company company = companyService.findByCompanyName("name", value, connection)
                     .orElseThrow(()-> new NoSuchElementException(String.format("Company %s does not exists!", value)));

             if (company == null){
                 String message = String.format("Company %s not exist", value);
                 throw  new ParameterException(message);
             }
        } catch (SQLException e) {
            Logger.getLogger(CompanyServiceImpl.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
