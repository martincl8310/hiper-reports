package com.tsvetkov.jcommander.validation;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class MonthValidator implements IParameterValidator {
  @Override
  public void validate(String name, String value) throws ParameterException {
    final int month = Integer.parseInt(value);
    if (month < 1 || month > 12) {
      String message = String.format("Month %s is invalid", value);
      throw new ParameterException(message);
    }
  }
}
