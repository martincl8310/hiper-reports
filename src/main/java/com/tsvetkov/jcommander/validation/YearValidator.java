package com.tsvetkov.jcommander.validation;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.time.Year;
import java.time.format.DateTimeFormatter;

public class YearValidator implements IParameterValidator {
  @Override
  public void validate(String name, String value) throws ParameterException {

    Year year = Year.parse(value, DateTimeFormatter.ofPattern("yyyy"));
    if (year.getValue() < 1970) {
      String message = String.format("Year %s is invalid", value);
      throw new ParameterException(message);
    }
  }
}
