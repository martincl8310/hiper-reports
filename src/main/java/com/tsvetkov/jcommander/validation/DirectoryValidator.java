package com.tsvetkov.jcommander.validation;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DirectoryValidator implements IParameterValidator {

  public void validate(String name, String value) throws ParameterException {
    Path pathToConfigDir = Paths.get(value);
    if (!exists(pathToConfigDir)) {
      createDirectory(pathToConfigDir);
    }
    if (!Files.isDirectory(pathToConfigDir, LinkOption.NOFOLLOW_LINKS)) {
      String message =
          String.format("The [%s] directory specified [%s] is not a directory: ", name, value);
      throw new ParameterException(message);
    }
    if (!checkPermissions(pathToConfigDir)) {
      String message =
          String.format(
              "Application does not have read & write permissions to [%s] directory [%s]",
              name, value);
      throw new ParameterException(message);
    }
  }

  private boolean checkPermissions(Path path) {
    return (Files.isReadable(path) && Files.isWritable(path));
  }

  private boolean exists(Path path) {
    return (Files.exists(path, LinkOption.NOFOLLOW_LINKS));
  }

  private void createDirectory(Path pathToConfigDir) {
    String exportDirPath = pathToConfigDir.toString();
    if (exportDirPath.charAt(exportDirPath.length() - 1) != '/') {
      exportDirPath = pathToConfigDir.toString() + '/';
    }
    File directory = new File(exportDirPath);
    boolean mkdir = directory.mkdir();
    if (!mkdir) {
      String message = String.format("The directory [%s] does not exist: ", exportDirPath);
      throw new ParameterException(message);
    }
  }
}
