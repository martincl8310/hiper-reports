package com.tsvetkov.jcommander;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.tsvetkov.configuration.DirectoryProperties;
import com.tsvetkov.constant.AppConstants;
import com.tsvetkov.jdbc.FlyWayMigration;
import com.tsvetkov.report.impl.*;
import com.tsvetkov.service.impl.CompanyDataProcessor;
import com.tsvetkov.sftpDownloader.SftpClient;
import com.tsvetkov.sftpDownloader.SftpClientDetails;
import com.tsvetkov.sftpDownloader.SftpClientImpl;
import com.tsvetkov.util.impl.DBTableCreatorImpl;
import com.tsvetkov.util.impl.XlsxParser;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.flywaydb.core.Flyway;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandLineParser {
  private CommandlineParameters parameters;
  private final DirectoryProperties dataDirectories;
  private final PropertiesConfiguration propConfiguration;
  private final CompanyDataProcessor companyDataProcessor;
  private final XlsxParser parser;
  private final Report report;
  private final ReceiptReport receiptReport;
  private final InvoiceReport invoiceReport;
  private final StoreReport storeReport;
  private final PaymentReport paymentReport;
  private final Flyway migration;

  public CommandLineParser() throws ConfigurationException {
    this.dataDirectories = DirectoryProperties.getInstance();
    this.propConfiguration = new PropertiesConfiguration(AppConstants.DIRECTORY_PROPERTIES);
    this.companyDataProcessor = new CompanyDataProcessor();
    this.parser = XlsxParser.getInstance();
    this.report = Report.getInstance();
    this.receiptReport = new ReceiptReport();
    this.invoiceReport = new InvoiceReport();
    this.storeReport = new StoreReport();
    this.paymentReport = new PaymentReport();
    this.migration = FlyWayMigration.getInstance();
  }

  public void parse(String[] args) throws SQLException {
    this.parameters = new CommandlineParameters();
    JCommander jCommander = new JCommander(parameters);
    jCommander.setProgramName("hyper-reports");

    try {
      jCommander.parse(args);
    } catch (ParameterException e) {
      Logger.getLogger(CommandLineParser.class.getName()).log(Level.SEVERE, e.getMessage());
    }

    if (parameters.isHelp()) {
      jCommander.usage();
      return;
    }
    if (parameters.isConfig()) {
      if (parameters.getImportDir() != null) {
        setImportedDir(parameters.getImportDir());
      }
      if (parameters.getExportDir() != null) {
        setExportedDir(parameters.getExportDir());
      }
    }
    if (parameters.isProcess()) {
      this.migration.migrate();
      downloadFilesFromSftpServer();
      companyDataProcessor.processData(dataDirectories.getInImportDirectory());
      System.out.println("Download complete!");
    }
    if (parameters.isReport()) {
      validateParameters();
      exportData();
    }
  }

  public void exportData() throws SQLException {
    List<Integer> months = parameters.getMonths();

    if (parameters.getAggregation().equals("receipt")) {
      if (parameters.getMonths().size() == 1){
        receiptReport.createMonthlyReport(months, parameters.getYear(), parameters.getCompany(), parameters.getOrder(), parameters.getTop());
      }else {
        this.receiptReport.createQuarterAndYearReport(months, parameters.getYear(), parameters.getCompany(), parameters.getOrder(), parameters.getTop());
      }
    }
    if (parameters.getAggregation().equals("invoice")){
      if (parameters.getMonths().size() == 1){
        invoiceReport.createMonthlyReport(months, parameters.getYear(), parameters.getCompany(), parameters.getOrder(), parameters.getTop());
      }else {
       invoiceReport.createQuarterAndYearReport(months, parameters.getYear(), parameters.getCompany(), parameters.getOrder(), parameters.getTop());
      }
    }
    if (parameters.getAggregation().equals("store")){
      if (parameters.getMonths().size() == 1){
        storeReport.createMonthlyReport(months, parameters.getYear(), parameters.getCompany(), parameters.getOrder(), parameters.getTop());
      }else {
        storeReport.createQuarterAndYearReport(months, parameters.getYear(), parameters.getCompany(), parameters.getOrder(), parameters.getTop());
      }
    }
    if (parameters.getAggregation().equals("payment")){
      if (parameters.getMonths().size() == 1){
        paymentReport.createMonthlyReport(months, parameters.getYear(), parameters.getCompany(), parameters.getOrder(), parameters.getTop());
      }else {
        paymentReport.createQuarterAndYearReport(months, parameters.getYear(), parameters.getCompany(), parameters.getOrder(), parameters.getTop());
      }
    }

    try {
      parser.xlsxReport(report, "hyper-reports");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  public void setParameters(CommandlineParameters parameters) {
    this.parameters = parameters;
  }

  private void validateParameters() {
    if (parameters.getYear() == 0
        || parameters.getCompany() == null
        || parameters.getAggregation() == null) {
      System.out.println("Company, year and aggregation cannot be empty!");
      System.exit(0);
    }
        if (parameters.getMonths() == null || parameters.getMonths().size() > 1){
          validateQuarterParams(parameters.getQuarter());
        }

  }

  private void setImportedDir(String directory) {
    try {
      propConfiguration.setProperty("importDirectory", directory);
      propConfiguration.save();
    } catch (ConfigurationException e) {
      Logger.getLogger(CommandLineParser.class.getName()).log(Level.SEVERE, e.getMessage());
    }
  }

  private void setExportedDir(String directory) {
    try {
      propConfiguration.setProperty("exportDirectory", directory);
      propConfiguration.save();
    } catch (ConfigurationException e) {
      Logger.getLogger(CommandLineParser.class.getName()).log(Level.SEVERE, e.getMessage());
    }
  }

  private void downloadFilesFromSftpServer() {
    if (dataDirectories.getInImportDirectory().isEmpty()) {
      System.out.println("Import data directory is not set!");
    } else {
      try {
         String path = "";
        if (dataDirectories.getInImportDirectory().charAt(dataDirectories.getInImportDirectory().length() - 1) != '/') {
          path = dataDirectories.getInImportDirectory()  + '/';
        }
        SftpClientDetails details =
            new SftpClientDetails("sftpuser", "hyperpass", "fe.ddns.protal.biz", 22, "/xml-data");
        SftpClient sftpClient = new SftpClientImpl(details);
        sftpClient.downloadFiles(path);
      } catch (Exception e) {
        Logger.getLogger(DBTableCreatorImpl.class.getName())
            .log(Level.SEVERE, "Sftp connection problem", e);
        System.exit(0);
      }
    }
  }

  private void validateQuarterParams(String quarter) {
    if (quarter == null){
      parameters.setMonths(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));
      return;
    }
    quarter = quarter.toLowerCase();
    switch (quarter) {
      case "q1":  parameters.setMonths(Arrays.asList(1, 2, 3)); break;
      case "q2":  parameters.setMonths(Arrays.asList(4, 5, 6)); break;
      case "q3":  parameters.setMonths(Arrays.asList(7, 8, 9)); break;
      case "q4": parameters.setMonths(Arrays.asList(10, 11, 12)); break;
      default: break;
    }
  }
}
