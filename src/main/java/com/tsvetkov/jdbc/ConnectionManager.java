package com.tsvetkov.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionManager {

  private ConnectionManager() {}


  public static Optional<Connection> getConnection() {

    try {
      Connection connection = DriverManager.getConnection(
              DbProperties.getJDBCUrl(), DbProperties.getJDBCUser(), DbProperties.getJDBCPassword());

      return Optional.ofNullable(connection);
    } catch (SQLException e) {
      Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, e);
    }
    return  Optional.empty();
  }

}
