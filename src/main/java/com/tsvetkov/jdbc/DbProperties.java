package com.tsvetkov.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbProperties {
  private static final Properties properties = new Properties();

  private static final DbProperties instance = new DbProperties();

  private DbProperties() {
    try (InputStream inputStream =
        DbProperties.class.getClassLoader().getResourceAsStream("db/dbconfig.properties")) {
      {
        properties.load(inputStream);
      }
    } catch (IOException e) {
      Logger.getLogger(DbProperties.class.getName()).log(Level.SEVERE, null, e);
    }
  }

  public static String getJDBCDriver() {
    return Optional.of(properties.getProperty("jdbc.driver")).orElseThrow(IllegalStateException::new);
  }

  public static String getJDBCUrl() {
    return Optional.of(properties.getProperty("jdbc.url")).orElseThrow(IllegalStateException::new);
  }

  public static String getJDBCUser() {
    return Optional.of(properties.getProperty("jdbc.user")).orElseThrow(IllegalStateException::new);
  }

  public static String getJDBCPassword() {
    return Optional.of(properties.getProperty("jdbc.password")).orElseThrow(IllegalStateException::new);
  }


  public static DbProperties getInstance() {
    return instance;
  }
}
