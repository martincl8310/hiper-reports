package com.tsvetkov.jdbc;

import org.flywaydb.core.Flyway;

public final class FlyWayMigration {
    private FlyWayMigration() {}
    public static Flyway getInstance(){
        return Flyway.configure()
                .dataSource(DbProperties.getJDBCUrl(), DbProperties.getJDBCUser(), DbProperties.getJDBCPassword())
                .load();
    }
}
