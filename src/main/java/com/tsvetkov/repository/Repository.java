package com.tsvetkov.repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface Repository<T> {

  Optional<T> persist(T entity, Connection connection);

  Optional<T> findByField(String columnName, Object columnValue, Connection connection) throws SQLException;

  List<T> findAllById(Integer id, Connection connection);
}
