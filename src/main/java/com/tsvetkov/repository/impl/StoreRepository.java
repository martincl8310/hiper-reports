package com.tsvetkov.repository.impl;

import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.model.entity.Store;
import com.tsvetkov.util.sqlutils.SqlUtil;
import com.tsvetkov.util.sqlutils.utils.QueryInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StoreRepository extends JdbcRepository<Store> {

  public StoreRepository(Class<Store> storeClass) {
    super(storeClass);
  }

  public List<Store> findStoresByCompany(int id) {
    Connection connection = ConnectionManager.getConnection().orElseThrow();
    SqlUtil sqlUtil = new SqlUtil();
    QueryInfo selection = sqlUtil.selection("store", "company_id", "EQ");
    List<Store> result = new ArrayList<>();
    String query = selection.getQuery();
    Map<Integer, Object> placeholders = new HashMap<>();
    placeholders.put(1, id);
    PreparedStatement statement = null;
    try {
      statement = connection.prepareStatement(query);
      for (Map.Entry<Integer, Object> entry : placeholders.entrySet()) {
        statement.setObject(entry.getKey(), entry.getValue());
      }
      final ResultSet resultSet = statement.executeQuery();
      while (resultSet.next()) {
        int storeId = resultSet.getInt(1);
        String name = resultSet.getString(2);
        String address = resultSet.getString(3);

        result.add(new Store(storeId, name, address));
      }
      return result;

    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }

    return null;
  }
}
