package com.tsvetkov.repository.impl;

import com.tsvetkov.model.entity.BaseEntity;
import com.tsvetkov.repository.Repository;
import com.tsvetkov.template.impl.TemplateImpl;
import com.tsvetkov.util.sqlutils.SqlUtil;
import com.tsvetkov.util.sqlutils.utils.QueryInfo;

import java.sql.*;
import java.util.*;

public class JdbcRepository<T extends BaseEntity> implements Repository<T> {

  private static final String ID = "id";

  private final Class<T> tClass;
  private final SqlUtil sqlUtil;

  public JdbcRepository(Class<T> tClass) {
    this.tClass = tClass;
    sqlUtil = new SqlUtil();
  }

  public Optional<T> persist(T entity, Connection connection) {
    Map<String, Object> data = new TemplateImpl<>(tClass).convertEntityToRow(entity);

    QueryInfo insert =
        sqlUtil.insert(tClass.getSimpleName().toLowerCase(), new ArrayList<>(data.keySet()));
    String query = insert.getQuery();
    try (PreparedStatement statement =
        connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS); ) {
      int count = 1;
      for (Object value : data.values()) {
        statement.setObject(count, value);
        count++;
      }
      statement.executeUpdate();
      ResultSet generatedKeys = statement.getGeneratedKeys();
      if (generatedKeys.next()) {
        entity.setId(generatedKeys.getInt(1));
      }
      return Optional.of(entity);

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return Optional.empty();
  }

  @Override
  public List<T> findAllById(Integer id, Connection connection) {
    QueryInfo selection =
        sqlUtil.selection(tClass.getSimpleName().toLowerCase(), "company_id", "EQ");

    String query = selection.getQuery();
    Map<Integer, Object> placeholders = new HashMap<>();
    placeholders.put(1, id);
    final List<T> all =
        new TemplateImpl<>(tClass).mapRowToAllEntity(connection, query, placeholders);
    return all;
  }

  @Override
  public Optional<T> findByField(String columnName, Object columnValue, Connection connection) {
    QueryInfo selection = sqlUtil.selection(tClass.getSimpleName().toLowerCase(), columnName, "EQ");

    String query = selection.getQuery();
    Map<Integer, Object> placeholders = new HashMap<>();
    placeholders.put(1, columnValue);
    Optional<T> entity = new TemplateImpl<>(tClass).mapRowToEntity(connection, query, placeholders);

    return entity;
  }
}
