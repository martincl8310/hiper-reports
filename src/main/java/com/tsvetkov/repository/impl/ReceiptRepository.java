package com.tsvetkov.repository.impl;

import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.model.entity.Receipt;
import com.tsvetkov.util.sqlutils.SqlUtil;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReceiptRepository extends JdbcRepository<Receipt> {
  private SqlUtil sqlUtil;
  private List<Receipt> result;

  public ReceiptRepository(Class<Receipt> receiptClass) {
    super(receiptClass);
    this.sqlUtil = new SqlUtil();
    this.result = new ArrayList<>();
  }

  public List<Receipt> findByDateAndStoreId(int year, int month, int storeId) {

    String query = sqlUtil.selectByYearAndMonth("receipt", "store").getQuery();
    ResultSet resultSet = null;
    try (Connection connection = ConnectionManager.getConnection().orElseThrow();
            PreparedStatement statement = connection.prepareStatement(query); ) {
      statement.setInt(1, year);
      statement.setInt(2, month);
      statement.setInt(3, storeId);

      resultSet = statement.executeQuery();
      while (resultSet.next()) {
        BigDecimal total = resultSet.getBigDecimal(1);
        result.add(new Receipt(total));
      }

    } catch (SQLException е) {
      е.printStackTrace();
    }finally{
      if (resultSet != null){
        try {
          resultSet.close();
        } catch (SQLException e) {
          Logger.getLogger(ReceiptRepository.class.getName()).log(Level.SEVERE, e.getMessage());
        }
      }
    }
    return result;
  }

  public List<Receipt> findByDayOfMonth(int year, int month, int day, int storeId) {
    String query = sqlUtil.selectByDayOfMonth("receipt", "store").getQuery();
    ResultSet resultSet = null;
    try (Connection connection = ConnectionManager.getConnection().orElseThrow();
            PreparedStatement statement = connection.prepareStatement(query)) {
      statement.setInt(1, year);
      statement.setInt(2, month);
      statement.setInt(3, day);
      statement.setInt(4, storeId);

      resultSet = statement.executeQuery();
      while (resultSet.next()) {
        BigDecimal total = resultSet.getBigDecimal(1);
        result.add(new Receipt(total));
      }
    } catch (SQLException е) {
      е.printStackTrace();
    }finally{
      if (resultSet != null){
        try {
          resultSet.close();
        } catch (SQLException e) {
          Logger.getLogger(ReceiptRepository.class.getName()).log(Level.SEVERE, e.getMessage());
        }
      }
    }
    return result;
  }
  public List<Receipt> findByPayment(int year, int month, String payment, int companyId){
    String query = sqlUtil.selectionByPaymentPerMonth("receipt", "store", "company").getQuery();
    ResultSet resultSet = null;
    try (Connection connection = ConnectionManager.getConnection().orElseThrow();
            PreparedStatement statement = connection.prepareStatement(query)) {
      statement.setInt(1, year);
      statement.setInt(2, month);
      statement.setString(3, payment);
      statement.setInt(4, companyId);

      resultSet = statement.executeQuery();
      while (resultSet.next()) {
        BigDecimal total = resultSet.getBigDecimal(1);
        result.add(new Receipt(total));
      }

    } catch (SQLException е) {
      е.printStackTrace();
    }finally{
      if (resultSet != null){
        try {
          resultSet.close();
        } catch (SQLException e) {
          Logger.getLogger(ReceiptRepository.class.getName()).log(Level.SEVERE, e.getMessage());
        }
      }
    }
    return result;
  }
  public List<Receipt> findByPaymentPerDayAndMonth(int year, int month,int day, String payment, int companyId){
    String query = sqlUtil.selectionByPaymentPerMonthAndDay("receipt", "store", "company").getQuery();
    ResultSet resultSet = null;
    try (Connection connection = ConnectionManager.getConnection().orElseThrow();
            PreparedStatement statement = connection.prepareStatement(query)) {
      statement.setInt(1, year);
      statement.setInt(2, month);
      statement.setInt(3, day);
      statement.setString(4, payment);
      statement.setInt(5, companyId);

      resultSet = statement.executeQuery();
      while (resultSet.next()) {
        BigDecimal total = resultSet.getBigDecimal(1);
        result.add(new Receipt(total));
      }

    } catch (SQLException е) {
      е.printStackTrace();
    }finally{
      if (resultSet != null){
        try {
          resultSet.close();
        } catch (SQLException e) {
          Logger.getLogger(ReceiptRepository.class.getName()).log(Level.SEVERE, e.getMessage());
        }
      }
    }
    return result;
  }
}
