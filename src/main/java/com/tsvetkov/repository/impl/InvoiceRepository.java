package com.tsvetkov.repository.impl;

import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.util.sqlutils.SqlUtil;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InvoiceRepository extends JdbcRepository<Invoice> {
    private SqlUtil sqlUtil;
    private List<Invoice> result;

    public InvoiceRepository(Class<Invoice> invoiceClass) {
        super(invoiceClass);
        this.sqlUtil = new SqlUtil();
        this.result = new ArrayList<>();
    }

    public List<Invoice> findByDateAndStoreId(int year, int month, int storeId) {
        String query = sqlUtil.selectByYearAndMonth("invoice", "store").getQuery();
        Connection connection = ConnectionManager.getConnection().orElseThrow();
        try (PreparedStatement statement = connection.prepareStatement(query); ) {
            statement.setInt(1, year);
            statement.setInt(2, month);
            statement.setInt(3, storeId);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                BigDecimal total = resultSet.getBigDecimal(1);
                result.add(new Invoice(total));
            }

        } catch (SQLException е) {
            е.printStackTrace();
        }
        return result;
    }

    public List<Invoice> findByDayOfMonth(int year, int month, int day, int storeId) {
        String query = sqlUtil.selectByDayOfMonth("invoice", "store").getQuery();

        try (Connection connection = ConnectionManager.getConnection().orElseThrow();
                PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, year);
            statement.setInt(2, month);
            statement.setInt(3, day);
            statement.setInt(4, storeId);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                BigDecimal total = resultSet.getBigDecimal(1);
                result.add(new Invoice(total));
            }

        } catch (SQLException е) {
            е.printStackTrace();
        }
        return result;
    }
    public List<Invoice> findByPaymentPerDay(int year, int month,int day, String payment, int companyId){
        String query = sqlUtil.selectionByPaymentPerMonthAndDay("invoice", "store", "company").getQuery();
        Connection connection = ConnectionManager.getConnection().orElseThrow();
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, year);
            statement.setInt(2, month);
            statement.setInt(3,day);
            statement.setString(4, payment);
            statement.setInt(5, companyId);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                BigDecimal total = resultSet.getBigDecimal(1);
                result.add(new Invoice(total));
            }

        } catch (SQLException е) {
            е.printStackTrace();
        }
        return result;
    }
    public List<Invoice> findByPaymentPerMonth(int year, int month, String payment, int companyId){
        String query = sqlUtil.selectionByPaymentPerMonth("invoice", "store", "company").getQuery();
        Connection connection = ConnectionManager.getConnection().orElseThrow();
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, year);
            statement.setInt(2, month);
            statement.setString(3, payment);
            statement.setInt(4, companyId);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                BigDecimal total = resultSet.getBigDecimal(1);
                result.add(new Invoice(total));
            }

        } catch (SQLException е) {
            е.printStackTrace();
        }
        return result;
    }
}
