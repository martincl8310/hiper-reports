package com.tsvetkov.repository.impl;

import com.tsvetkov.constant.Sql.DatabaseSchemaCreator;
import com.tsvetkov.exception.EntityPersistFailed;
import com.tsvetkov.jdbc.ConnectionManager;
import com.tsvetkov.model.entity.Company;
import com.tsvetkov.model.entity.Invoice;
import com.tsvetkov.util.sqlutils.SqlUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CurrentDateByCompanyRepository {
  private final SqlUtil sqlUtil = new SqlUtil();

  public void persistCompanyNameAndNewDate(String date, String company) {
    final String query =
        sqlUtil
            .insert(DatabaseSchemaCreator.LAST_REPORT_DATES_BY_COMPANY,
                    Arrays.asList("local_date","company_name"))
            .getQuery();
    try (Connection connection = ConnectionManager.getConnection().orElseThrow(SQLException::new);
        PreparedStatement statement =
            connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS); ) {

      statement.setDate(1, Date.valueOf(date));
      statement.setString(2, company);
      statement.executeUpdate();
    } catch (SQLException e) {
      Logger.getLogger(Invoice.class.getName()).log(Level.SEVERE, e.getMessage(), e);
    }
  }

  public String lastReportDayByCompany(String companyName) {
    String query =
        sqlUtil
            .selection(DatabaseSchemaCreator.LAST_REPORT_DATES_BY_COMPANY, "company_name", "EQ")
            .getQuery();
    List<String> result = new ArrayList<>();
    ResultSet resultSet = null;
    try (Connection connection = ConnectionManager.getConnection().orElseThrow(SQLException::new);
        PreparedStatement statement = connection.prepareStatement(query); ) {

      statement.setString(1, companyName);
      resultSet = statement.executeQuery();

      while (resultSet.next()) {
        String date = resultSet.getString("local_date");
        result.add(date);
      }
    } catch (SQLException e) {
      Logger.getLogger(Company.class.getName()).log(Level.SEVERE, null, e);
    }finally{
      if (resultSet != null){
        try {
          resultSet.close();
        } catch (SQLException e) {
          Logger.getLogger(Company.class.getName()).log(Level.SEVERE, null, e);
        }
      }
    }
    return result.get(0);
  }
  public void updateCompanyLastReportDate(String companyName, String newDate)
          throws EntityPersistFailed {
   String query =  sqlUtil .update(DatabaseSchemaCreator.LAST_REPORT_DATES_BY_COMPANY,
                    "local_date","company_name", "EQ")
            .getQuery();
    try (Connection connection = ConnectionManager.getConnection().orElseThrow(SQLException::new);
         PreparedStatement statement = connection.prepareStatement(query)) {
      statement.setString(1, newDate);
      statement.setString(2, companyName);

      int rowCount = statement.executeUpdate();
      if (rowCount == 0) {
        throw new EntityPersistFailed("Fail to persist an entity");
      }
    } catch (SQLException e) {
      Logger.getLogger(Company.class.getName()).log(Level.SEVERE, null, e);
    }
  }
  public List<String> companyNamesWithCurrentDateReport() {
    List<String> result = new ArrayList<>();
    try (Connection connection = ConnectionManager.getConnection().orElseThrow(SQLException::new);
         PreparedStatement statement =
                 connection.prepareStatement(
                         DatabaseSchemaCreator.COMPANY_NAMES_WITH_CURRENT_DAY_REPORT);
         ResultSet resultSet = statement.executeQuery(); ) {

      while (resultSet.next()) {
        String companyName = resultSet.getString("company_name");
        result.add(companyName);
      }
    } catch (SQLException e) {
      Logger.getLogger(Company.class.getName()).log(Level.SEVERE, null, e);
    }
    return result;
  }
}
