CREATE TABLE IF NOT EXISTS company
(
    id      INT PRIMARY KEY AUTO_INCREMENT,
    address VARCHAR(200) NOT NULL,
    `name`  VARCHAR(100) NOT NULL,
    `uuid`  VARCHAR(200) NOT NULL unique
);

CREATE TABLE IF NOT EXISTS store
(
    id         INT PRIMARY KEY AUTO_INCREMENT,
    `name`     VARCHAR(100) NOT NULL unique,
    address    VARCHAR(200) NOT NULL,
    company_id INT,
    CONSTRAINT FK_store_company FOREIGN KEY (company_id) REFERENCES company (id)
);

CREATE TABLE IF NOT EXISTS card
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    card_type   varchar(100),
    `number`    VARCHAR(100) UNIQUE,
    contactless TINYINT(1)
);

CREATE TABLE IF NOT EXISTS receipt
(
    id         INT PRIMARY KEY AUTO_INCREMENT,
    total      DECIMAL(7, 2),
    local_date TIMESTAMP,
    payment    VARCHAR(20),
    store_id   INT,
    card_id    INT,
    CONSTRAINT FK_receipt_store FOREIGN KEY (store_id) REFERENCES store (id),
    CONSTRAINT FK_receipt_card_id FOREIGN KEY (card_id) REFERENCES card (id)
);

CREATE TABLE IF NOT EXISTS customer
(
    id      INT PRIMARY KEY AUTO_INCREMENT,
    `name`  VARCHAR(50),
    address VARCHAR(200),
    `uuid`  VARCHAR(200) UNIQUE
);

CREATE TABLE IF NOT EXISTS invoice
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    `total`     DECIMAL(15, 2),
    local_date  DATE,
    payment     VARCHAR(20),
    store_id    INT,
    customer_id INT,
    card_id     INT,
    CONSTRAINT FK_invoice_store FOREIGN KEY (store_id) REFERENCES store (id),
    CONSTRAINT FK_invoice_customer FOREIGN KEY (customer_id) REFERENCES customer (id),
    CONSTRAINT FK_invoice_card_details FOREIGN KEY (card_id) REFERENCES card (id)
);

CREATE TABLE IF NOT EXISTS last_report_dates_by_company
(
    id           INT PRIMARY KEY auto_increment,
    local_date   date,
    company_name varchar(50) unique
);